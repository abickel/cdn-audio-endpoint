<?php

?>

<script>


	function updateDisplayedActivities()
	{
		 $(document).on('click', '#refresh_activities', function() {
			 var input = $("#activity-input").val();
		 });
	}
function handleActivityCheckboxes() {

/*change learnosity activities being shown*/
    $(document).on('click', '#refresh_activities', function() {

        var activitySet = $("input[name=lrn_items]", "#active");
        var numElems = activitySet.length,
            querystring = "?activeItems=";

        activitySet.each(function(i, e) {

            if ($(this).prop("checked")) {
                querystring += $(this).val();
                querystring += ":";
            }
        });
        window.location.href = querystring;
    });

    /*select 'all' learnosity activity toggle */
	/*
    $(document).on('click', "#all-box", function() {

        var activitySet = $("input[name=lrn_items]", "#active");
        var checked = $(this).prop("checked");
        if (checked) {
            activitySet.each(function(i, e) {
                $(this).prop("checked", true);
            });
        } else {
            activitySet.each(function(i, e) {
                $(this).prop("checked", false);
            });
        }
    });
*/
}



function menuAnimation()
{
	$(".lrn_menu_item").on('click', function(){

		var target = $("#" + $(this).find(".lrn_itm_list").data("toggle"));
		target.toggle();
		//target.show();
	});
}
/*
function menuAnimation() {
    /*show / hide menu options
    $(".lrn_itm_list").on('click', function() {
        var $selected = $(this);

        $(".learnosity_menu").find(".lrn_itm_list").each(function() {

            if ($(this).data("toggle") !== $selected.data("toggle")) {
                var target = $(this).data("toggle");
                $("#" + target).hide();
            }
        });
        var op = $selected.data("toggle");
        $("#" + op).toggle();
    });
}
*/

function populateAudioVoiceIDselectBox() {
    /*get list of available voice IDs for upload*/
    var payload = { "getAudioID": "true" };
    $.post("services/serve.php", payload, function(response) {
        var data = $.parseJSON(response),
            target = $("#voice_id_options"),
            default_option = $("<option value='select'>Select Voice</option>");
        target.append(default_option);

        for (var i in data) {
            profile = data[i];
            var opt = $('<option value="' + profile.id + '">' + profile.voice + ' - ' + profile.lang + ' - ' + profile.gender + '</option>');
            target.append(opt);
        }
    });
}

/*client / page load actions*/
$(function(){

	$(".closer").on('click', function(){

		var target = $("#"+$(this).data("target"));
		console.log(target);
		target[0].hide();
		});


/*update list of available reports*/

	$("#export_menu").on('click', function(){

		generateReportsToView();
	});

/*generate report file*/
	$("#export_file").on('click', function(){
		var reportType = $('input[name=type]:checked', '#reportType').val();

		var payload = {"rtype": reportType};

				$.blockUI({ message: $("#exportMsg") });
    		$("#exportMsg").css("display", "block");

		$.post("services/export.php", payload, function(response){

			console.log(response);
			
			var data = $.parseJSON(response);

			if(data.status === "success")
			{
				var fileName = data.file,
				 	reportType = data.type;

			
				generateReportsToView();

				window.location.href = "exports/"+reportType+"/"+fileName;
			}
			$.unblockUI();
        	$("#exportMsg").css("display", "hidden");
		});
	});

	function generateReportsToView()
	{

		if($("#collapse0").is(":visible"))
			return;
		$.blockUI({ message: $("#reportMsg") });
    	$("#reportMsg").css("display", "block");

		var payload = {"reportList": "true"};
		$.post("services/serve.php", payload, function(response){

				var data = $.parseJSON(response);
				$("#report_container").empty();

				for(var key in data)
				{
					var obj = data[key];

					for(var k in obj)
					{
						var reports = obj[k];
						var container = $('<div class="report-category">'+k+':</div>');
						$("#report_container").append(container);

						for(var m in reports)
						{
							container.append('<br><a href="'+reports[m].path+'/'+reports[m].name+'">'+reports[m].date+'</a>');
						}
					}
				}
				$.unblockUI();
        		$("#reportMsg").css("display", "hidden");
		});
	}




	/*Add new voice ID*/
	$("#new_voice_id_confirm").on('click', function(){
		if($("#newVoiceId").val().length < 3)
		{
			alert("Invalid Voice ID");
			return;
		}
		else
		{
			var id = $("#newVoiceId").val(),
				language = $('input[name=language]:checked', '#voice_id_language').val(),
				gender =  $('input[name=gender]:checked', '#voice_id_gender').val(),
				payload = {"id": id, "language": language, "gender": gender, "uaID": "true"};

			$.blockUI({ message: $("#newVoiceID") });
    		$("#newVoiceID").css("display", "block");

			$.post("services/upload.php", payload, function(response) {
				$.unblockUI();
        		$("#newVoiceID").css("display", "hidden");
			});
		}
	});

	/*upload Audio File*/
	$("#upload_new_audio_file").on('click', function(){



	});

					 /*update file color-labeling */
	 $("#recording_status").on('click', function(){
	 /*push array of md5/ language to server to determine if we have a recorded file of any type. */	

	 var mode = $('input[name=view_status]:checked', '#recording_status_opts').val(); //view only OR update DB	based on user selection
		refreshAllItemsAssociation(md5Array, mode);
	 });
});
</script>




<div class="learnosity_menu col-xs-12">


	<div class="lrn_menu_item">
		<div  class="lrn_itm_list" data-toggle="addVoiceID">
		    	    <h4 >
        	    Add Voice ID
      	    </h4>
		</div>
		<div class="dropdown" id="addVoiceID">
			<span class="closer" data-target="addVoiceID">X</span>
			<input id="newVoiceId" type="text" placeholder="new voice id" style="color: black"><br>
			<form id="voice_id_gender">
				<input type="radio" name="gender" value="f" checked="checked"> Female </input>
        		<input type="radio" name="gender" value="m">Male</input>  
			</form>
			<br>
			<form id="voice_id_language">
            	<input type="radio" name="language" value="e" checked="checked"> English</input>
            	<input type="radio" name="language" value="s">Spanish</input>
        	</form>
			<br>
			<div class="panel-footer">
			<button id="new_voice_id_confirm" class="menu-button">Add ID</button>
			</div>
		</div>
	</div>

	<div class="lrn_menu_item">
		<div  class="lrn_itm_list" data-toggle="uploadFile">
		    	    <h4 >
        	    Upload Audio
      	    </h4>
		</div>
		<div class="dropdown" id="uploadFile" >
		<form enctype="multipart/form-data" action="services/upload.php" method="post">
		<div class="override-container">Allow Overwrite
			<input type="radio" name="allow_override" value="true" > Yes </input>
        	<input type="radio" name="allow_override" value="false" checked="checked">No</input> 
		</div>
			<h4>
			Select Voice ID
			</h4>
			<select id="voice_id_options" name="voice_id_options" style="color: black" ></select>	

			<h4>
			Select File
			</h4>
			<input type="file"style="color: green" name="target_file[]" multiple="multiple"></input>
			<br>
			<div class="panel-footer">
			<input type="submit" id="upload_new_audio_file" name="upload_audio_button"style="color: black"></input>
			</div>
			</form>
		</div>
	</div>


    <div class="lrn_menu_item">
	    <div  class="lrn_itm_list" data-toggle="collapse0" id="export_menu">
    	    <h4 >
        	    Reporting
      	    </h4>
        </div>
        <div class="dropdown" id="collapse0">
		<div class="panel" style="color:black;" ><h4>Download Reports</h4>
		<div id="report_container"></div>
		</div>
		<div class="panel" style="color:black"><h4>Create Reports</h4></div>
				<form id="reportType">
					<input type="radio" name="type" value="all_missing" checked="checked"> All Missing Recordings </input>    		
        		</form>
				<br>
			<div class="panel-footer"><button id="export_file" class="menu-button">Generate</button></div>
		</div>
    </div>


    <div class="lrn_menu_item"  >
	    <div  class="lrn_itm_list" data-toggle="collapse1">
    	    <h4 >
        	    Learnosity Activities
      	    </h4>
        </div>
        <div class="dropdown activities" id="collapse1">

			<input type="text" id="activity-input" placeholder="Activity id, comma seperated..."></input>
		</div>
		 
    </div>


	

    <div class="lrn_menu_item"  >
	    <div  class="lrn_itm_list" data-toggle="collapse4">
    	    <h4 >
        	    Recording Status
      	    </h4>
        </div>
		
        <div class="dropdown"id="collapse4" >
			<span class="closer" data-target="collapse4">X</span>
			<br>
			<form id="recording_status_opts"> 
			<input type="radio" class="recording-radio" name="view_status" value="update" >Update DB</input> 
			<input type="radio" class="recording-radio" name="view_status" value="view" checked="checked">View Only</input> 		
			</form>
			<br>
 			<button id="recording_status" class="menu-button">Go!</button>
			 <br>
  			<button id="key-toggle" class="menu-button" data-toggle="color-key">Show Key</button>
			<div  id="color-key">
	        	<div class="recorded color-display">Recorded Audio</div><br>
	        	<div class="tts color-display">TTS Only</div><br>
	        	<div class="none color-display">No File</div><br>
			</div>

        </div>
    </div>

</div>