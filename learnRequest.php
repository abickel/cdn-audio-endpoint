<?php

$defaultQapi = 'v2';

$get_learnosity_activities = (isset($_GET["activeItems"]) &&(!empty($_GET["activeItems"])));
/*collect selected activities*/
if($get_learnosity_activities)
{
	$selectedActivities = filter_input(INPUT_GET, "activeItems", FILTER_SANITIZE_STRING);
	$activities = explode(":", $selectedActivities);
	$filtered = [];
	$maxIndex = count($activities) -1;
	$index = 0;
	while($index < $maxIndex)
	{
		$filtered[] = $activities[$index];
		$index++;
	}
	$activities = $filtered;

foreach($activities as $activity)
{
	$requestD = 
	array(
	'references' => array($activity)
);
}


?>



<head> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>
    /**
     * Sends an XHR request to an end-user server
     * passing all request parameters that will go
     * to the Data API.
     * @param  {object} request The request object
     * being passed to the Data API
     * @return void
     */
    function signRequest (request) {
        $.ajax({
            url: './signrequest.php',
            method: 'POST',
            data: {'request': request}
        }).done(function(data, status, xhr) {
            sendRequest(JSON.parse(data));
        }).fail(function(xhr, status, error) {
            console.log('Error: ', xhr, status, error);
        });
    }

    /**
     * Sends an XHR to the Data API with a fully signed
     * request.
     * @param  {object} signedRequest The `security`, `request`
     * and `action` keys to be POSTed to the Data API
     * @return void
     */
    function sendRequest (signedRequest) {
        var url = 'https://data.learnosity.com/latest/itembank/items';

        $.ajax({
            url: url,
            method: 'POST',
            data: signedRequest
        }).done(function(data, status, xhr) {
            processResults(data);
        }).fail(function(xhr, status, error) {
            console.log('Error: ', xhr, status, error);
        });
    }

    /**term
     * Do something with the results...
     * @param  {object} results The Data API response
     */
    function processResults (results) {
        console.log(results);
    }

    $(function() {
        // An object of request data
        var request = {
            'limit': 5
        };

    <?php
    $request = array(
    'activity_id'    => substr($prefix.$domain."itemsassessdemo2", 0, 36),
    'name'           => 'live progress test',
    'rendering_type' => 'inline',
    //'state'          => $itemState,
    //'type'           => $itemState == 'review'? 'local_practice': 'submit_practice',
    'course_id'      => 'testCourse',
    //'session_id'     => Uuid::generate(),
    //'user_id'        => $student['id'],
    "items"			 => $items,
    'config'         => array(
        'questionsApiVersion' => 'v2',
		"questions_api_init_options" => [
        "math_renderer"=> "mathquill"
		]
    )
);
    ?>
        signRequest(<?php echo json_encode($request) ?>);

    });
</script>
</head>