var audioi = document.createElement('audio');
var sourcei = document.createElement('source');
var language = "e";
//var grade = 'adv1';
//var ie8sound;
var isiPad = navigator.userAgent.match(/iPad/i) != null;
var isiPod = navigator.userAgent.match(/iPod/i) != null;


//These are set to MS preferences. Empty arrays will yeild no results. CDN structure requires us to know the voice ID that we are seeking
var engVoice = ['1', '2', '3', '4'],
    spanVoice = ['5', '6', '7', '8', '9', '10']; //arrays in HS order

/*master 'onload' */
$(document).ready(function() {

    $('#left-side-content').load('http://www.bigideasmath.com/protected/content/dc/start_menu.html');
    redoAudioLinks();
});


/**padding */
function zeroFill(number, width) {
    width -= number.toString().length;
    if (width > 0) {
        return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
    }
    return number;
}


/**convert all playAudio() calls to updated CDN friendly format */
function redoAudioLinks() {
    //This needs to occur, but there's an issue with the table builder for some reason. The $('.popup_word') doesn't find it's parent in audioHashTableBuilder.js
    //$('.popup_word').each(function(){$(this).parent().prepend($(this).text()); $(this).remove();}); 
    $.each($('a[href^=javascript\\:play_audio]'), function() {

        var oldID = $(this).context.href.replace('javascript:play_audio(', '').replace(')', '').replace(/['";]+/g, ''),
            fileInfo = getFileHash($(this).text()).str,

            /*build an identity to track each item's physical source if we need to log missing data */
            location = window.location.href;
        identity = location + ":" + oldID;

        var data = { "md5": fileInfo, "text": $(this).text(), "identity": identity };

        console.log(identity);
        $(this).context.href = "javascript:play_audio_hash('" + fileInfo + "','" + $(this).text() + "','" + identity + "')";
        $(this).addClass('oldLink-' + oldID);

    });
};


/**CDN enabled content delivery of audio files, based on the selected voice array */
function play_audio_hash(hash, text, identity) {

    var data = {};
    data.md5 = hash;
    data.text = text;
    data.identity = identity;
    data.voiceArray = (language === "e") ? engVoice : spanVoice;
    data.arrayIndex = 0;

    ebook_cdn_seek(data);

}

/**CDN enabled content delivery of audio files, based on the selected voice array */
function ebook_cdn_seek(data) {

    var shortHash = data.md5[0] + data.md5[1] + data.md5[2];

    var baseurl = "http://68b9c1bc7c9fa35e4a6d-0264cef6aad1b81efe617b95ad9e9064.r0.cf2.rackcdn.com";

    /* if we are still within array bounds, check CDN for next index*/
    if (!(data.arrayIndex === data.voiceArray.length)) {

        var url = baseurl + "/" + shortHash + "/" + data.voiceArray[data.arrayIndex] + "/" + data.md5 + ".mp3";

        /*check this url */
        $.ajax({
            url: url,
            type: 'head',
            dataType: 'application/json',
            crossDomain: true,
            success: function(response) {

                loadAudio(url);
            },
            error: function(response) {
                data.arrayIndex = (data.arrayIndex + 1); //increment the voice array index

                ebook_cdn_seek(data) //check next index in array 
            }
        });
        /*voice array exhausted, now check tts */
    } else {
        /*we dont have the desired audio recording for this voice id, so we need to make a note of it in the audio_files_needed table */
        var payload = { "post_missing_recording": "true", "md5": data.md5, "text": data.text, "activity": data.identity, "voiceID": data.voiceArray[0] };

        console.log(payload);
        $.post("log/logmissing.php", payload, function(response) {
            console.log(response);
            alert("Sorry, no audio available :(");
        });
    }
}

/*push audio to player */
function loadAudio(url) {

    var player = audioi;
    mp3 = document.createElement("source");
    mp3.src = url;

    player.pause(); //in case of latent play state

    while (player.firstChild) { //empty the player
        player.removeChild(player.firstChild);
    }

    player.appendChild(mp3); //add new source
    player.load(); //restart

    player.play(); //play audio
}


/**toggle selected language and the associated voice ID array */
function change_language(lid) {
    $('.audio').removeClass("selected");
    $('.audio > img').attr('src', "images/audio_off.png");
    switch (lid) {
        case 1:
            language = "e";
            $('#english_selected_a').addClass("selected");
            $('#english_selected').attr('src', 'images/audio_on.png');
            break;
        case 2:
            language = "s";
            $('#spanish_selected_a').addClass("selected");
            $('#spanish_selected').attr('src', 'images/audio_on.png');
            break;
        default:
            language = "e";
    }
}

function scroll_page(dir) {
    curPage = curPage + dir;
    if (curPage <= 0) { curPage = 1; }
    if (curPage >= maxPages) { curPage = maxPages; }
    document.location.hash = "#page" + curPage;
}

function scroll_to_page(pn) {
    document.location.hash = "#page" + pn;
}


/***************************************
 ****************UTILITY****************
 ***************************************/

/**
 *
 * MD5 (Message-Digest Algorithm)
 * http://www.webtoolkit.info/
 *
 **/
var MD5 = function(string) {
    function RotateLeft(lValue, iShiftBits) {
        return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
    }

    function AddUnsigned(lX, lY) {
        var lX4,
            lY4,
            lX8,
            lY8,
            lResult;
        lX8 = (lX & 0x80000000);
        lY8 = (lY & 0x80000000);
        lX4 = (lX & 0x40000000);
        lY4 = (lY & 0x40000000);
        lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
        if (lX4 & lY4) {
            return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
        }
        if (lX4 | lY4) {
            if (lResult & 0x40000000) {
                return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
            } else {
                return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
            }
        } else {
            return (lResult ^ lX8 ^ lY8);
        }
    }

    function F(x, y, z) {
        return (x & y) | ((~x) & z);
    }

    function G(x, y, z) {
        return (x & z) | (y & (~z));
    }

    function H(x, y, z) {
        return (x ^ y ^ z);
    }

    function I(x, y, z) {
        return (y ^ (x | (~z)));
    }

    function FF(a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function GG(a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function HH(a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function II(a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function ConvertToWordArray(string) {
        var lWordCount;
        var lMessageLength = string.length;
        var lNumberOfWords_temp1 = lMessageLength + 8;
        var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
        var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
        var lWordArray = Array(lNumberOfWords - 1);
        var lBytePosition = 0;
        var lByteCount = 0;
        while (lByteCount < lMessageLength) {
            lWordCount = (lByteCount - (lByteCount % 4)) / 4;
            lBytePosition = (lByteCount % 4) * 8;
            lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount) << lBytePosition));
            lByteCount++;
        }
        lWordCount = (lByteCount - (lByteCount % 4)) / 4;
        lBytePosition = (lByteCount % 4) * 8;
        lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
        lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
        lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
        return lWordArray;
    };

    function WordToHex(lValue) {
        var WordToHexValue = "",
            WordToHexValue_temp = "",
            lByte,
            lCount;
        for (lCount = 0; lCount <= 3; lCount++) {
            lByte = (lValue >>> (lCount * 8)) & 255;
            WordToHexValue_temp = "0" + lByte.toString(16);
            WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
        }
        return WordToHexValue;
    };

    function Utf8Encode(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    };
    var x = Array();
    var k,
        AA,
        BB,
        CC,
        DD,
        a,
        b,
        c,
        d;
    var S11 = 7,
        S12 = 12,
        S13 = 17,
        S14 = 22;
    var S21 = 5,
        S22 = 9,
        S23 = 14,
        S24 = 20;
    var S31 = 4,
        S32 = 11,
        S33 = 16,
        S34 = 23;
    var S41 = 6,
        S42 = 10,
        S43 = 15,
        S44 = 21;
    string = Utf8Encode(string);
    x = ConvertToWordArray(string);
    a = 0x67452301;
    b = 0xEFCDAB89;
    c = 0x98BADCFE;
    d = 0x10325476;
    for (k = 0; k < x.length; k += 16) {
        AA = a;
        BB = b;
        CC = c;
        DD = d;
        a = FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
        d = FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
        c = FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
        b = FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
        a = FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
        d = FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
        c = FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
        b = FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
        a = FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
        d = FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
        c = FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
        b = FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
        a = FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
        d = FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
        c = FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
        b = FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
        a = GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
        d = GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
        c = GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
        b = GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
        a = GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
        d = GG(d, a, b, c, x[k + 10], S22, 0x2441453);
        c = GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
        b = GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
        a = GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
        d = GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
        c = GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
        b = GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
        a = GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
        d = GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
        c = GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
        b = GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
        a = HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
        d = HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
        c = HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
        b = HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
        a = HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
        d = HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
        c = HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
        b = HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
        a = HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
        d = HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
        c = HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
        b = HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
        a = HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
        d = HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
        c = HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
        b = HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
        a = II(a, b, c, d, x[k + 0], S41, 0xF4292244);
        d = II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
        c = II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
        b = II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
        a = II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
        d = II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
        c = II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
        b = II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
        a = II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
        d = II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
        c = II(c, d, a, b, x[k + 6], S43, 0xA3014314);
        b = II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
        a = II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
        d = II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
        c = II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
        b = II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
        a = AddUnsigned(a, AA);
        b = AddUnsigned(b, BB);
        c = AddUnsigned(c, CC);
        d = AddUnsigned(d, DD);
    }
    var temp = WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d);
    return temp.toLowerCase();
}

var trim = function(string) {
    return string.replace(/^\s+|\s+$/gm, '');
}

var sanitize = function(string) {
    var str = trim(string);
    //Using http://unicode-search.net/unicode-namesearch.pl
    //replace all unicode dash (minus) types with "hyphen-minus" '-'
    str = str.replace(/[\u00AD\u05BE​\u1806\u2010\u2011\u2012​\u2013\u2014\u2015\u2212\uFE58\uFE63\uFF0D]/g, '\u002D');
    //replace Full-width Plus with Plus
    str = str.replace(/[\uFF0B\u2795]/g, '\u002b');
    //replace versions of multiplication signs with
    str = str.replace(/[\u2715\u2716]/g, '\u00D7');
    //replace double quotes with standard double quotes
    str = str.replace(/[\u201C\u201D\u201F\u275D\u275D\u275E\u301D\u301E\uFF02\u2033]/g, '\u0022');
    //replace double spaces with single spaces
    str = str.replace(/\s\s+/g, ' ');
    //replace curly apostrophes and prime with apostrophes
    str = str.replace(/[\u02BC\u055A\u07F4\u07F5\uFF07\u2018\u2019\u201B\u275B\u275C\u2032]/g, '\u0027');
    //replace elipses with ...
    str = str.replace(/[\u2026]/g, '...');
    //replace i acute with i
    str = str.replace(/[\u00CD\u00ED]/g, 'i');
    //replace Combining Left Arrow above with left arrow
    str = str.replace(/[\u20D6]/g, '\u2190');
    //replace Combining Right Arrow above with right arrow
    str = str.replace(/[\u20D7]/g, '\u2192');
    //replace left-pointing angle bracket with <
    str = str.replace(/[\u2329\u27E8\u3008]/g, '<');
    //replace right-pointing angle bracket with >
    str = str.replace(/[\u232A\u27E9\u3009]/g, '>');
    //replace goofy f character with the letters fl
    str = str.replace(/\uFB02\ /g, 'fl');
    str = str.replace(/\uFB02/g, 'fl');
    //replace goofy fi character with the letters fi

    str = str.replace(/\uFB01\ /g, 'fi');
    str = str.replace(/\uFB01/g, 'fi');
    //WE MAY OR MAY NOT WANT TO DO THIS
    t1 = testString(str);
    str = removePrivateUse(str);
    t2 = testString(str);
    return { 'str': str, 'notes': { 'testBeforeRemovingPrivateUse': t1, 'testAfterRemovingPrivateUse': t2 } };
}

var removePrivateUse = function(str) {
    //converts all private use unicode characters to '<|pu|>'
    //replace private use with <pu>
    s = str.replace(/[\uE000-\uF8FF]/g, '<|pu|>');
    return s;
}

var zeroFill = function(number, width) {
    width -= number.toString().length;
    if (width > 0) {
        return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
    }
    return number;
}

var getFileHash = function(string) {
    var s = sanitize(string)
    return { 'str': MD5(s.str), 'notes': s.notes };
}

var testString = function(string) {
    // \u03C3 sigma
    // \u00B1 Plus-Minus Sign
    // \u00BA Masculine Ordinal Indicator
    // \u00A0 nonBreakingSpace
    // \u2200-\u22FF are math symbols
    // \u00B0 is the degree sign. Considering it legit for now. 
    // \u00D7 is multiplication sign. Considering legit for now.
    // \u2022 is a bullet. Considering legit.
    // \u2113 script small l considered legal?
    // \u005E circumflex accent '^' considered legal
    // \u25B3 white up-pointing triangle - considered legal, or at least no good sub
    // \u2190-2193 arrows - considered legal, or at least no good sub
    // \u00E9 LATIN SMALL LETTER E WITH ACUTE - considered valid
    // \u03A9 omega considered valid
    // \u0391-\u03A9 \u03B1-\u03C4 Greek letters - legit
    // {} | < > considered legal

    //
    if (/^[a-zA-Z0-9- "\[\]\\*:%$.,;!\/_\(\)\n\r{}|<>=?+π\-\u00F7\u2200-\u22FF\u03A9\u03B1-\u03C4\u2113\u0391-\u03A9\u00E9\u2190-\u2193\u25B3\u005E\u00D7\u2022\u03C3\u00B1\u2032\u2033\u2026\u00BA\u00A0\u00B0\u0027\u0022\u002b\u002d\u00b7\u2018\u2019\u201B\u275B\u275C]*$/.test(string) == false) {
        var s = string.replace(/[a-zA-Z0-9- "\[\]\\*:%$.,;!\/_\(\)\n\r{}|<>=?+π\-\u00F7\u2200-\u22FF\u03A9\u03B1-\u03C4\u2113\u0391-\u03A9\u00E9\u2190-\u2193\u25B3\u005E\u00D7\u2022\u03C3\u00B1\u2032\u2033\u2026\u00BA\u00A0\u00B0\u0027\u0022\u002b\u002d\u00b7\u2018\u2019\u201B\u275B\u275C]/g, '');
        var ch = {};
        for (var i = 0, len = s.length; i < len; i++) {
            ch[s[i]] = s.charCodeAt(i);
        }

        return ch;
    }
    return false;
}