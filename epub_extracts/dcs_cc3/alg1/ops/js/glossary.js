[
	{
		"term":"power",
		"type":0,
		"definition":"A product of repeated factors"
	},

	{
		"term":"base",
		"type":0,
		"definition":"The base of a power is the repeated factor."
	},

	{
		"term":"exponent",
		"type":0,
		"definition":"The exponent of a power indicates the number of times the base is used as a factor."
	}
]


[
	{
		"term":"perfect square",
		"type":0,
		"definition":"The square of a whole number"
	}
]

[
	{
		"term":"numerical expression",
		"type":0,
		"definition":"An expression that contains only numbers and operations"
	},

	{
		"term":"evaluate",
		"type":0,
		"definition":"Use the order of operations to find the value of a numerical expression."
	},

	{
		"term":"order of operations",
		"type":0,
		"definition":"The order in which to perform operations when evaluating expressions with more than one operation"
	}
]

[
	{
		"term":"factor pair",
		"type":0,
		"definition":"Two whole numbers other than zero that are multiplied together to get a product"
	},

	{
		"term":"prime factorization",
		"type":0,
		"definition":"A composite number written as the product of its prime factors"
	},

	{
		"term":"factor tree",
		"type":0,
		"definition":"A diagram that shows the prime factorization of a number"
	}
]

[
	{
		"term":"Venn diagram",
		"type":0,
		"definition":"A diagram that uses circles to describe relationships between two or more sets"
	}
]

[
	{
		"term":"common factors",
		"type":0,
		"definition":"Factors that are shared by two or more numbers"
	},

	{
		"term":"greatest common factor",
		"type":0,
		"definition":"The greatest of the common factors of two or more numbers"
	}
]

[
	{
		"term":"common multiples",
		"type":0,
		"definition":"Multiples that are shared by two or more numbers."
	},

	{
		"term":"least common multiple",
		"type":0,
		"definition":"The least of the common multiples of two or more numbers"
	}
]

[
	{
		"term":"least common denominator",
		"type":0,
		"definition":"The least common multiple of the denominators of two or more fractions"
	}
]

[
	{
		"term":"reciprocals",
		"type":0,
		"definition":"Two numbers whose product is 1"
	}
]

[
	{
		"term":"algebraic expression",
		"type":0,
		"definition":"An expression that contains numbers, operations, and one or more symbols"
	},

	{
		"term":"terms",
		"type":0,
		"definition":"The parts of an algebraic expression"
	},

	{
		"term":"variable",
		"type":0,
		"definition":"A symbol that represents one or more numbers"
	},

	{
		"term":"coefficient",
		"type":0,
		"definition":"The numerical factor of a term that contains a variable"
	},

	{
		"term":"constant",
		"type":0,
		"definition":"A term without a variable"
	}
]

[
	{
		"term":"equivalent expressions",
		"type":0,
		"definition":"Expressions with the same value"
	}
]

[
	{
		"term":"like terms",
		"type":0,
		"definition":"Terms of an algebraic expression that have the same variables raised to the same exponents"
	}
]

[
	{
		"term":"factoring an expression",
		"type":0,
		"definition":"Writing a numerical expression or algebraic expression as a product of factors"
	}
]

[
	{
		"term":"polygon",
		"type":0,
		"definition":"A closed figure in a plane that is made up of three or more line segments that intersect only at their endpoints"
	}
]

[
	{
		"term":"composite figure",
		"type":0,
		"definition":"A figure made up of triangles, squares, rectangles, semicircles, and other two-dimensional figures."
	}
]

[
	{
		"term":"ratio",
		"type":0,
		"definition":"A comparison of two quantities; The ratio of a to b can be written as a : b."
	}
]

[
	{
		"term":"equivalent ratios",
		"type":0,
		"definition":"Two ratios that describe the same relationship"
	},

	{
		"term":"ratio table",
		"type":0,
		"definition":"A table used to find and organize equivalent ratios"
	}
]

[
	{
		"term":"rate",
		"type":0,
		"definition":"A ratio of two quantities using different units"
	},

	{
		"term":"unit rate",
		"type":0,
		"definition":"A rate that compares a quantity to one unit of another quantity"
	},

	{
		"term":"equivalent rates",
		"type":0,
		"definition":"Rates that have the same unit rate"
	}
]

[
	{
		"term":"percent",
		"type":0,
		"definition":"A part-to-whole ratio where the whole is 100"
	}
]

[
	{
		"term":"U.S. customary system",
		"type":0,
		"definition":"System of measurement that contains units for length, capacity, and weight"
	},

	{
		"term":"metric system",
		"type":0,
		"definition":"Decimal system of measurement, based on powers of 10, that contains units for length, capacity, and mass"
	},

	{
		"term":"conversion factor",
		"type":0,
		"definition":"A rate that equals 1; A conversion factor is used to convert units."
	},

	{
		"term":"unit analysis",
		"type":0,
		"definition":"A process used to decide which conversion factor will produce the appropriate units"
	}
]

[
	{
		"term":"positive numbers",
		"type":0,
		"definition":"Numbers that are greater than 0"
	},

	{
		"term":"negative numbers",
		"type":0,
		"definition":"Numbers that are less than 0"
	},

	{
		"term":"opposites",
		"type":0,
		"definition":"Two numbers that are the same distance from 0 on a number line, but on opposite sides of 0"
	},

	{
		"term":"integers",
		"type":0,
		"definition":"The set of whole numbers and their opposites"
	}
]

[
	{
		"term":"absolute value",
		"type":0,
		"definition":"The distance between a number and 0 on a number line; The absolute value of a number a is written as | a |."
	}
]

[
	{
		"term":"coordinate plane",
		"type":0,
		"definition":"A coordinate plane is formed by the intersection of a horizontal number line and a vertical number line."
	},

	{
		"term":"origin",
		"type":0,
		"definition":"The point, represented by the ordered pair (0, 0), where the horizontal and vertical numbers lines intersect in a coordinate plane"
	},

	{
		"term":"quadrants",
		"type":0,
		"definition":"The four regions created by the intersection of the horizontal and the vertical number lines in a coordinate plane"
	}
]

[
	{
		"term":"equation",
		"type":0,
		"definition":"A mathematical sentence that uses an equal sign, =, to show that two expressions are equal"
	}
]

[
	{
		"term":"solution",
		"type":0,
		"definition":"A value that makes an equation true"
	}
]

[
	{
		"term":"inverse operations",
		"type":0,
		"definition":"Operations that ?undo? each other, such as addition and subtraction or multiplication and division"
	}
]

[
	{
		"term":"equation in two variables",
		"type":0,
		"definition":"An equation that represents two quantities that change in relationship to one another"
	},

	{
		"term":"solution of an equation in two variables",
		"type":0,
		"definition":"An ordered pair that makes an equation in two variables true"
	},

	{
		"term":"independent variable",
		"type":0,
		"definition":"The variable representing the quantity that can change freely in an equation in two variables"
	},

	{
		"term":"dependent variable",
		"type":0,
		"definition":"The variable whose value depends on the independent variable in an equation in two variables"
	}
]

[
	{
		"term":"inequality",
		"type":0,
		"definition":"A mathematical sentence that compares expressions; It contains the symbols <, >, <=, or >=."
	}
]

[
	{
		"term":"solution of an inequality",
		"type":0,
		"definition":"A value that makes an inequality true"
	},

	{
		"term":"solution set",
		"type":0,
		"definition":"The set of all solutions of an inequality"
	}
]

[
	{
		"term":"graph of an inequality",
		"type":0,
		"definition":"A graph that shows all the solutions of an inequality on a number line"
	}
]

[
	{
		"term":"solid",
		"type":0,
		"definition":"A three-dimensional figure that encloses a space"
	},

	{
		"term":"polyhedron",
		"type":0,
		"definition":"A solid whose faces are all polygons"
	},

	{
		"term":"face",
		"type":0,
		"definition":"A flat surface of a polyhedron"
	},

	{
		"term":"edge",
		"type":0,
		"definition":"A line segment where two faces intersect"
	},

	{
		"term":"vertex",
		"type":0,
		"definition":"A point where three or more edges intersect"
	},

	{
		"term":"prism",
		"type":0,
		"definition":"A polyhedron that has two parallel, identical bases; The lateral faces are parallelograms."
	},

	{
		"term":"pyramid",
		"type":0,
		"definition":"A polyhedron that has one base; The lateral faces are triangles."
	}
]

[
	{
		"term":"surface area",
		"type":0,
		"definition":"The sum of the areas of all the faces of a solid"
	},

	{
		"term":"net",
		"type":0,
		"definition":"A two-dimensional representation of a solid"
	}
]

[
	{
		"term":"volume",
		"type":0,
		"definition":"A measure of the amount of space that a three-dimensional figure occupies; Volume is measured in cubic units such as cubic feet (ft^3 ) or cubic meters (m^3 )."
	}
]

[
	{
		"term":"statistics",
		"type":0,
		"definition":"The science of collecting, organizing, analyzing, and interpreting data"
	},

	{
		"term":"statistical question",
		"type":0,
		"definition":"A question for which you do not expect to get a single answer"
	}
]

[
	{
		"term":"mean",
		"type":0,
		"definition":"The sum of the data divided by the number of data values"
	}
]

[
	{
		"term":"outlier",
		"type":0,
		"definition":"A data value that is much greater or much less than the other values"
	}
]

[
	{
		"term":"measure of center ",
		"type":0,
		"definition":"A measure that describes the typical value of a data set"
	},

	{
		"term":"median",
		"type":0,
		"definition":"For a data set with an odd number of ordered values, the median is the middle value. For a data set with an even number of ordered values, the median is the mean of the two middle values."
	},

	{
		"term":"mode",
		"type":0,
		"definition":"The data value or values that occur most often; Data can have one mode, more than one mode, or no mode."
	}
]

[
	{
		"term":"measure of variation",
		"type":0,
		"definition":"A measure that describes the distribution of a data set"
	},

	{
		"term":"range",
		"type":0,
		"definition":"The difference between the greatest value and the least value of a data set"
	},

	{
		"term":"quartiles",
		"type":0,
		"definition":"The quartiles of a data set divide the data into four equal parts."
	},

	{
		"term":"first quartile",
		"type":0,
		"definition":"The median of the lower half of a data set"
	},

	{
		"term":"third quartile",
		"type":0,
		"definition":"The median of the upper half of a data set"
	},

	{
		"term":"interquartile range",
		"type":0,
		"definition":"The difference between the third quartile and the first quartile of a data set; represents the range of the middle half of the data"
	}
]

[
	{
		"term":"mean absolute deviation",
		"type":0,
		"definition":"An average of how much data values differ from the mean"
	}
]

[
	{
		"term":"stem-and-leaf plot",
		"type":0,
		"definition":"A type of data display that uses the digits of data values to organize a data set; Each data value is broken into a stem (digit or digits on the left) and a leaf (digit or digits on the right)."
	},

	{
		"term":"stem",
		"type":0,
		"definition":"Digit or digits on the left of a stem-and-leaf plot"
	},

	{
		"term":"leaf",
		"type":0,
		"definition":"Digit or digits on the right of a stem-and-leaf plot"
	}
]

[
	{
		"term":"frequency table",
		"type":0,
		"definition":"A table used to group data values into intervals."
	},

	{
		"term":"frequency",
		"type":0,
		"definition":"The number of data values in an interval"
	}
]

[
	{
		"term":"histogram",
		"type":0,
		"definition":"A bar graph that shows the frequency of data values in intervals of the same size; The height of a bar represents the frequency of the values in the interval. There are no spaces between bars."
	}
]

[
	{
		"term":"box-and-whisker plot",
		"type":0,
		"definition":"A type of graph that represents a data set along a number line by using the least value, the greatest value, and the quartiles of the data." 
	},

	{
		"term":"five-number summary",
		"type":0,
		"definition":"The five numbers that make up a box-and-whisker plot"
	}
]
