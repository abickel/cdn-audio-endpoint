jQuery(function($) {
	
	$("a.popup_word").click(function() {
			var curid = $( this ).attr('id');
			loadPopup(curid);
	return false;
	});

	$("div.closeglossary").click(function() {
	var parentid = $(this).parent().attr('id');
		disablePopup(parentid);
	});
	

	 /************** start: functions. **************/

	var popupStatus = 0;
	
	function loadPopup(curid) {
		if(popupStatus == 0) {
		$('#popup_container').find('#'+curid).fadeIn();
			popupStatus = 1;
		}
	}

	function disablePopup(parentid) {
		if(popupStatus == 1) {
			$('#popup_container').find('#'+parentid).fadeOut();
			popupStatus = 0;
		}
	}
	/************** end: functions. **************/
});

