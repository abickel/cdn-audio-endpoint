<?php

$voiceID = filter_input(INPUT_POST, 'voiceID', FILTER_SANITIZE_STRING);                        
$activity  = filter_input(INPUT_POST, 'activity', FILTER_SANITIZE_STRING);                         
$text = filter_input(INPUT_POST, 'text', FILTER_SANITIZE_STRING);              
$md5 = filter_input(INPUT_POST, 'md5', FILTER_SANITIZE_STRING);                  
$postData  = filter_input(INPUT_POST, 'post_missing_recording', FILTER_SANITIZE_STRING);    



if(isset($postData))
{
        /*add hash/text to table audio_hash*/
        addHashTextToDB($md5, $text, $activity);
        
        /*log required audio hash to audio_files_needed*/
        addNeededAudioFileToLog($md5,$voiceID);
}




function addHashTextToDB($hash, $string, $source)
{
    $sql  = "INSERT IGNORE INTO `audio_hash` (`audio_hash_id`, `audio_text`, `notes`) VALUES (?, ?, ?)";
    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);

    $source = "Source: ".$source;
    
    $stmt->bind_param("sss", $hash, $string, $source);

    if(!$stmt->execute())
        die();
    
    $connection->_close();
}

function addNeededAudioFileToLog($hash, $preferredVoiceID)
{
    $sql = "INSERT IGNORE INTO `audio_files_needed` (`audio_hash_id`, `preferred_audio_voice_id`) VALUES (?,?)";

    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);
    
    $stmt->bind_param("si", $hash, $preferredVoiceID);

    if(!$stmt->execute())
        
    
    $connection->_close();
}


class DbConn
{
    private $link;
    private $stmt;

    public function __construct()
    {
        $db = "166.78.103.146";
        $user = "evotext";
        $pword = "Vu1Tkc409s09vV4zkVNF";
        $this->link =  mysqli_connect($db, $user, $pword, "audio");
    }


    public function _close()
    {
        $this->link->close();
    }

    public function _prepare($sql)
    {
        if(! $stmt =  $this->link->prepare($sql))
        {
             die('{"status": "sql_err", "message": "'.$this->link->error.'"}');
        }
        return $stmt;
    }

    public function link()
    {
        return $this->link;
    }
}
?>