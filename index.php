<?php 


session_start();
$_SESSION['LastAttemptedPage'] = "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
use LearnositySdk\Request\Init;
use LearnositySdk\Utils\Uuid;
include_once 'learnosityFiles/config.php';
use LearnositySdk\Request\DataApi;



if(isset($_GET["ItemBank"]) && $_GET["ItemBank"] = '2014'){
	$ck = $consumer_key;
	$cs = $consumer_secret;
}else {
	$ck = $k8consumer_key;
	$cs = $k8consumer_secret;
}

$security = array(
    'consumer_key' => $ck,
    'domain'       => $domain
);

$defaultQapi = 'v2';

$get_learnosity_activities = (isset($_GET["activeItems"]) &&(!empty($_GET["activeItems"])));
/*collect selected activities*/
if($get_learnosity_activities)
{
	$selectedActivities = filter_input(INPUT_GET, "activeItems", FILTER_SANITIZE_STRING);
	$activities = explode(":", $selectedActivities);
	$filtered = [];
	$maxIndex = count($activities) -1;
	$index = 0;
	while($index < $maxIndex)
	{
		$filtered[] = $activities[$index];
		$index++;
	}
	$activities = $filtered;
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Audio Mgm't</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="http://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
	<script src="//items.learnosity.com"></script> 
	<script src="js/audio.js"></script>
	<script src="js/md5.js"></script>
	<script src="js/main.js"></script>
	<script src="js/learnosity.js"></script>
	<script src="js/blockUI.js"></script>

	<link rel="stylesheet" type="text/css" href="./css/bi-style.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="css/tts.css">
  </head>

    <body class="bi-textured">


	<div class="container">
	<div class="bborder-top">
	<div class= "control-panel" >
	<?php include_once("menu.html");?>
	</div>
	
	<div id="fallback-anchor" class="resource-library"><!-- BEGIN MAIN CONTAINER -->
<?php 


/*collect learnosity items*/
$activityItems = array();
$activityItems["hasResults"] = false;
$activityItems["items"] = array();


foreach($activities as $activity)
{
	$requestD = 
	array(
	'references' => array($activity)
);

$action = 'get';
$dataApi = new DataApi();


$dataApi->requestRecursive(
    'https://data.learnosity.com/v0.50/itembank/activities',
    $security,
    $cs,
    $requestD,
    $action,
    function ($data) {

        myCallback($data);

		
    }
);
}


$GLOBALS['activityCount'] = 1;
$GLOBALS['activityItems'];



/*write learnosity items*/
function myCallback($fromLearn)
{
	$GLOBALS['itemCount'] ++;
	global $activityItems;

	if(!empty($fromLearn)){

		
		if(isset($fromLearn['data'][0])){
			foreach ($fromLearn['data'][0]['data']['items'] as $d){		//each learnosity item
			$GLOBALS['activityItems']["items"][] = $d;
			$GLOBALS['activityItems']["hasResults"] = true;
				echo '<h3>'.$d.'</h3><span class="learnosity-item" data-reference="'.$d.'"></span>';
			}
		}
	}
}
$items =$GLOBALS['activityItems']["items"];
    /*if we have learnosity items to process*/
    if(!empty($items)){
        

    if(isset($_GET['qApi']) && !empty($_GET['qApi'])){
	    $qApi = $_GET['qApi'];
    }else{
         
         $qApi = $defaultQapi;
    }
    if(isset($_GET['itemState']) && !empty($_GET['itemState'])){
        $itemState = $_GET['itemState'];
    }else{
        $itemState = 'review';
    } 
    if(isset($_GET['session']) && !empty($_GET['session'])){
        $session = $_GET['session'];
    }else{
        $session = Uuid::generate();
    } 

    $security = array(
        "consumer_key" => $ck,
        "domain"       => $domain,
        'timestamp'    => $timestamp
    );
    $prefix='';

    if(isset($_GET['prefix'])){
        $prefix = trim($_GET['prefix']);
    }	
    
    $student = array(
    'id'   => 'testUser2',
    'name' => 'Test User'
    );
	$request = array(
    'activity_id'    => substr($prefix.$domain."itemsassessdemo2", 0, 36),
    'name'           => 'live progress test',
    'rendering_type' => 'inline',
    'state'          => $itemState,
    'type'           => $itemState == 'review'? 'local_practice': 'submit_practice',
    'course_id'      => 'testCourse',
    'session_id'     => Uuid::generate(),
    'user_id'        => $student['id'],
    "items"			 => $items,
    'config'         => array(
        'questionsApiVersion' => 'v2',
		"questions_api_init_options" => [
        "math_renderer"=> "mathquill"
		]
    )
);



  }
    $Init = new Init('items', $security, $cs, $request);
    $signedRequest = $Init->generate(); 


?>
    <script>

	var md5Array = [],		//hold list of all md5 tags pulled from learnosity
       	eventOptions = {

        readyListener: function(e) {
			/*display text*/
		  	createStrings();


				/*testing sanitization*/
	for(var i in md5Array)
	{
		var entry = md5Array[i];
		var text = entry.text;
		var hash = entry.md5;

		var line = $('<p>hash: '+hash+'   text: '+text+'</p>');
		//$("body").append(line);
	}
        },
        errorListener: function(e) {
          // Adds a listener to all error
          console.log(e);
        }
      };


      /**/

$(document).ready(function(){



	/*contaner for  selected learnosity activities*/
	var activeItems = [];
	/*toggle learnosity activity fetch*/
	<?php	
	if($get_learnosity_activities)	{ ?>

		var itemsApp = LearnosityItems.init(<?php echo $signedRequest; ?>,eventOptions);

			$.blockUI({message: "Loading content... <img src='media/images/ajax-loader.gif'>" });
            
			$("#learnosity-menu-trigger").click();
		
	<?php	
		
		
	} ?>
	//handleActivityCheckboxes();

	//menuAnimation();

	/*old method of populating activities through json file.*/

    //getActivities(activeItems);

	populateAudioVoiceIDselectBox();
});



    function processResults (results) {
		console.log("sendRequest(): ");
        console.log(results);
    }


    function sendRequest (signedRequest) {


		console.log(signedRequest);

        var url = 'http://data.learnosity.com/v0.50/itembank/activities';

        $.ajax({
            url: url,
            method: 'POST',
            data: signedRequest
        }).done(function(data, status, xhr) {
            processResults(data);
        }).fail(function(xhr, status, error) {
            console.log('Error: ', xhr, status, error);
        });
    }

function updateHashNotes()
{
	var payload = { "md5": JSON.stringify(md5Array), "language": "e", "update": "true"};
	  $.post("services/serve.php", payload, function(response) {

			var data = $.parseJSON(response);
	  });

}

    </script>	
	  <div id="learnosity_assess"></div>
    </div>
	  

  </div>
 <audio id="audio_player" controls="controls"></audio>
  </div>
   
  </div><!-- END MAIN -->



 <!--display messages -->
    <div id="loading" class="message" style="display: none">
		Checking Files   <img src="media/images/ajax-loader.gif"></img>
	</div>


    	<div id="getAudio" class="message" style="display: none">
		Retrieving audio <img src="media/images/ajax-loader.gif"></img>
	</div>

	<div id="newVoiceID" class="message" style="display: none">
		Adding Voice ID <img src="media/images/ajax-loader.gif">
	</div>

	<div id="exportMsg" class="message" style="display: none">
		Generating Export File <img src="media/images/ajax-loader.gif">
	</div>

        <div id="reportMsg" class="message" style="display: none">
		Loading Reports...
	</div>

    <div id="fallbackMsg" style="display:none">Collecting Fallback Data...</div>
    <div id="fallbackSolutionMsg" style="display:none">Seeking Fallback Solutions...</div>
    <div id="ebookOverview" style="display:none">Gathering e-book data...</div>


<?php include_once("fallbackWindow.html"); ?>


  </body>
</html>
