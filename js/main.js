/*
function playAudio(md5, item_source) {

    var player = $("#audio_player"),
        language = $('input[name=language]:checked', '#lang').val(),
        text = $("#text_" + md5).text();
    var voiceIDarr = [];
    voiceIDarr[0] = 2;
    voiceIDarr[1] = 3;


    /*determine if audio exists for this element
    if (hasSource(player)) {
        var playHash = player.data("md5");

        if ((playHash === md5) && (languageMatches(player, language))) {
            player.trigger("play");
            return;
        }
    }
    /*need new audio file
    //parseAudio(player, language, gender, text, md5, learnosity_activity);
    var src = null;
    clientFindAudio(md5, language, voiceIDarr);

    // src = serverFindAudio(md5, text, language, voiceIDarr, item_source);


    // loadAudio(player, src);
}
*/


/*update element classes to display different colors based on type fo recording that exists for a file */
function refreshFileAssoc(md5, recordingType) {
    $(".text-container").each(function() {

        var target = $(this),
            targetHash = target.data("hash");

        if (targetHash === md5) {
            removeAssociationClasses(target); //remove prior associations
            target.addClass(recordingType);
        }
    });
}

/*collect file type association from server, and optionally update database with missing files if clicnt has selectied that option
    we don't care here, but we will pass the chouce along to the server.
*/
function refreshAllItemsAssociation(md5Array, serverMode, voiceIDarray) {

    /*lock out user while we load */
    $.blockUI({ message: $("#loading") });
    $("#loading").css("display", "block");


    /*no audio, or request for different file -  get new source */
    var payload = { "md5": JSON.stringify(md5Array), "assoc": "true", "idArray": JSON.stringify(voiceIDarray), "mode": serverMode };

    $.post("services/serve.php", payload, function(response) {

        console.log(response);

        var data = $.parseJSON(response);

        for (var i in data) {
            var entry = $.parseJSON(data[i]),
                md5 = entry.md5,
                type = entry.recordingType;

            refreshFileAssoc(md5, type);
        }
        $.unblockUI();
        $("#loading").css("display", "hidden");
    });
}

/*remove existing associated classes */
function removeAssociationClasses(elem) {

    elem.removeClass("recorded");
    elem.removeClass("tts");
    elem.removeClass("none");
}