/*attempt to locate audio content on the CDN, and fall back to AWS Polly if nothing found

    @playerID - i.d. of the audio player to feed.
    @languageRadioButtonName - name of radio button group
    @languageRadioFormID - id of form containing radio for language choice
    @text - the text to be spoken if no CDN resource found
    @md5 - the md5 of the text string
    @voice_id_array - array of acceptable voice ID's that are acceptable to return
                    - first index [0] is the one to be posted to database if we need to log this recording

                    /*be sure to update the language source on caller */
/* */
function deliverAudio(playerID, languageRadioButtonName, languageRadioFormID, text, md5, voice_id_array, activitySource) {

    var language = $('input[name=' + languageRadioButtonName + ']:checked', '#' + languageRadioFormID).val();

    /*trigger audio play and exit if file we need is already loaded */
    if (fileIsAlreadyLoaded(playerID, md5, language))
        return;

    /*need to look for file*/
    cdnSeek(playerID, language, text, md5, voice_id_array, 0, activitySource);
}

/**accept object from a successful $.AJAX call for our audio URL.
 * 
 * @player - the audio player object we need to add source to
 * @data - object created by cdnSeek() includes url, md5, language, and recording type
 */
function loadAudio(playerID, data) {

    var player = $("#" + playerID);
    mp3 = document.createElement("source");
    mp3.src = data.fileSource;
    mp3.dataset.md5 = data.md5;
    mp3.dataset.language = data.lang;

    player[0].pause(); //in case of latent play state
    player.empty(); //clear player sources
    player.append(mp3); //add new source
    player.load(); //restart
    player.trigger("play"); //play audio

    /**update appropriate container class to reflect the type of recording we have located */
    refreshFileAssoc(data.md5, data.type);
}

/*client tries to access same file twice in a row, no need to scour CDN for the pre-loaded file */
function fileIsAlreadyLoaded(playerID, md5, language) {
    var player = $("#" + playerID);
    var playHash = player.find("source").data("md5");
    var playLang = player.find("source").data("language");
    if ((playHash === md5) && (playLang === language)) {
        player[0].pause(); //in case of latent play state
        player.trigger("play");
        return true;

    }
    return false;
}


/*see if audio player has attached source elements */
function hasSource(obj) {
    obj.find("source").each(function() {
        return true;
    });
    return false;
}

/*do language of player & current selected language match? */
function languageMatches(obj, curLanguage) {
    obj.find("source").each(function() {
        if ($(this).data("language") === curLanguage)
            return true;
    });
    return false;
}


/**check cdn for desired file, iterate through acceptable voice id options provided by array
 * if no recorded file is found, then check the appropriate TTS directory to see if Polly has dont this one for us before. 
 * if all seeking fails, push request to server to gather fresh Polly recording.  
 * in any case, return the audio source, update the audio player, and trigger audio play.
 * 
 *  @playerID - i.d. of the audio player to feed.
    @language - the language to be search for (TTS files and Polly generation only)    { 'e' | 's'}
    @text - the text to be spoken if no CDN resource found
    @md5 - the md5 of the text string
    @voice_id_array - array of acceptable voice ID's that are acceptable to return
                    - first index [0] is the one to be posted to database if we need to log this recording
    @index - the currend index that we are checking (increment with each recursive call)
    @activity source - notation of the element/activity/question, etc.. that generated this text.  a reference point for logging in DB

    **IMPORTANT! any file being retrieved MUST have been uploaded to the CDN with the ''Access-Control-Allow-Origin' header enabled.
                    otherwise we will NOT be able to retrieve the file via $.AJAX query.
 */

function cdnSeek(playerID, language, text, md5, voiceIDarr, index, activitySource) {

    var shortHash = md5[0] + md5[1] + md5[2];

    var baseurl = "http://68b9c1bc7c9fa35e4a6d-0264cef6aad1b81efe617b95ad9e9064.r0.cf2.rackcdn.com";

    /* if we are still within array bounds, check CDN for next index*/
    if (!(index === voiceIDarr.length)) {

        /**derive url from md5 and voice id */
        var url = baseurl + "/" + shortHash + "/" + voiceIDarr[index] + "/" + md5 + ".mp3";
        //url = baseurl + "/" + shortHash + "/tts/" + language + "/" + md5 + ".mp3";






        /*check this url */
        $.ajax({
            url: url,
            type: 'head',
            /*prepare data structure to be passed to the audio player*/
            success: function(response) {

                var data = { "fileSource": url, "md5": md5, "lang": language, "type": "recorded" };

                loadAudio(playerID, data);
            },
            /*search for this index failed, try next index in array */
            error: function(response) {
                cdnSeek(playerID, language, text, md5, voiceIDarr, (index + 1), activitySource); //check next index in array 
            }
        });
        /*voice array exhausted, now check tts */
    } else {
        /*we dont have the desired audio recording for this voice id, so we need to make a note of it in the audio_files_needed table */
        var payload = { "post_missing_id": "true", "md5": md5, "text": text, "activity": activitySource, "voiceID": voiceIDarr[0] };

        $.post("services/serve.php", payload, function(response) {});

        var url = baseurl + "/" + shortHash + "/tts/" + language + "/" + md5 + ".mp3";
        $.ajax({
            url: url,
            crossDomain: true,
            type: 'GET',
            success: function(response) {

                var data = { "fileSource": url, "md5": md5, "lang": language, "type": "tts" };

                loadAudio(playerID, data);
            },
            /*no recording, no tts, now must fall back to polly.*/
            error: function(response) {
                var payload = { "md5": md5, "language": language, "string": text, "cdnFail": "true", "voiceID": voiceIDarr[0], "activity": activitySource };

                console.log("sending Id: " + voiceIDarr[0] + " to polly");

                $.post("services/serve.php", payload, function(response) {

                    console.log(response);

                    var data = $.parseJSON(response);

                    if (data.status == "found") {
                        /*format data */
                        var formattedData = { "fileSource": data.filename, "md5": md5, "lang": language, "type": data.recordingType };
                        loadAudio(playerID, formattedData);
                    } else {
                        console.log(data);
                    }

                });
            }
        });
    }
}