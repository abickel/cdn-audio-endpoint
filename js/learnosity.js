function createStrings() {

    var nbsp = "&nbsp;";
    /*gather pertinent data from learnosity items - text, activity id, etc.. */
    $('.lrn_stimulus').each(function(i, e) {
        var element = $(this);

        element.parents().each(function() {

            if ($(this).data("reference")) {
                var activity = $(this).data("reference");
                addAudioInfo(element, activity, false, 'orange');
            }
        });
    });
    $.unblockUI(); //learnosity has loaded, give control back to user
    $("#initMsg").css("display", "none");
}


/*generate & display text strings form learnosity data*/
/*generate audio button to trigger file retrival */
/*associate learnosity activity */
function addAudioInfo(obj, activity, removeMath = true, color = 'red') {

    var text = obj.clone().find('.MathJax,.lrn-accessibility-label').remove().end();


    $.each(text.find('.lrn_math_static'), function() {
        var mt = $(this).find('.lrn-display-offscreen').text();
        $(this).html(mt);
    });
    text.find('.lrn_textinput,.lrn_combobox').html('blank');

    // console.log(text.html());

    text.html(function(index, html) {
        return html.replace('&nbsp;', ' ');
    });

    text = text.text();

    var parsed = text.replace(/\u00A0/g, ' '); //kill hardcoded unicode-encoded(&nbsp;) spaces
    parsed = parsed.replace(/\s/g, ' '); //html encoded (&nbsp;) spaces
    parsed = parsed.replace(/\s{2,}/g, ' '); //kill big spaces

    var md5_String = MD5(parsed),
        data = { "md5": md5_String, "text": parsed, "activity": activity },
        voice_id_array = [9,12];


    if (md5Array.indexOf(data) == -1)
        md5Array.push(data); //collect each instance of md5 and its corresponding text

    if (text.replace(/\s/g, '') != '') {

        /*add play button for this */
        var playbtn = $("<img class=\"audio-play\" data-md5=\"" + md5_String + "\"  src=\"media/images/media-playback-start.png\">");
        playbtn.on('click', function() { deliverAudio("audio_player", "language", "voice_id_language", parsed, md5_String, voice_id_array, activity) });

        obj.append(playbtn);
        obj.append("<span id=\"text_" + md5_String + "\" class=\"text-speech\">" + parsed + "</span><p><span style=\"text-decoration: underline\">md5:</span> " + md5_String + "&nbsp</p>");
        obj.css('border', 'solid ' + color + ' 1px');
        obj.addClass("text-container");
        obj.data("hash", md5_String);
    }
}


function getActivities(activeArray) {

    $.getJSON("media/json/activities.json", function(data) {

        var target = $("#collapse1");
        var form = $('<form class="activity-form" id="active"></form>');

        for (var key in data.data) {
            var val = data.data[key]["activity"];

            var obj = $('<div class="activity-checkbox"><input type="checkbox" name="lrn_items" value="' + val + '">' + val + '</input></div>');
            if (activeArray.indexOf(val) > -1) {
                obj.find("input").prop("checked", true);
            }
            form.append(obj);
        }

        form.append($('<div class="activity-checkbox select-all"><input id="all-box" type="checkbox" name="lrn_items" value="all">All Activities</input></div>'));
        target.append(form);
        target.append('<div class="panel-footer"><button class="menu-button" id="refresh_activities">Update</button></div>');
    })
}