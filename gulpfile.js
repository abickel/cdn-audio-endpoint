'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var sftp = require('gulp-sftp');
var creds = require("./creds/techadmin.json");

/** Configuration **/
var user = creds.credentials.user;
var password = creds.credentials.pword;
var host = creds.credentials.host;
var port = 21;
var remoteFolder = '/var/www/html/audio_mgmt';

gulp.task('default', ['sftp-deploy-watch']);

var globs = [
    '!{.git/,.git/**}', //ignore git 
    '!{node_modules/,node_modules/**}', //dont need node packages
    '!{aws/,aws/**}', //ignore aws source
    '!{vendor/,vendor/**}', //ignore rackspace CDN source
    '!{learnosityFiles/,learnosityFiles/**}', //ignore learnosity source
    '!{exports/,exports/**}', //ignore generated report files
    '*',
    'css/*.css',
    'js/*.js',
    'services/**/*.php',
    'media/**/*'

];

gulp.task('sftp-deploy-watch', function() {
    gulp.watch(globs)
        .on('change', function(event) {
            console.log('Changes detected! Uploading file "' + event.path + '", ' + event.type);
            return gulp.src([event.path], { base: '.', buffer: false })
                .pipe(sftp({
                    host: host,
                    pass: password,
                    user: user,
                    remotePath: remoteFolder
                }));
        });
});