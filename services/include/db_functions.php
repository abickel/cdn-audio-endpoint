<?php


function addHashTextToDB($hash, $string, $source)
{
    $sql  = "INSERT IGNORE INTO `audio_hash` (`audio_hash_id`, `audio_text`, `notes`) VALUES (?, ?, ?)";
    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);

    $source = "Source: ".$source;
    
    $stmt->bind_param("sss", $hash, $string, $source);

    if(!$stmt->execute())
        die('{"status": "sql_err", "message": "unable to add audio to db"}');
    
    $connection->_close();
}

function addNeededAudioFileToLog($hash, $preferredVoiceID)
{
    $sql = "INSERT IGNORE INTO `audio_files_needed` (`audio_hash_id`, `preferred_audio_voice_id`) VALUES (?,?)";

    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);
    
    $stmt->bind_param("si", $hash, $preferredVoiceID);

    if(!$stmt->execute())
        die('{"status": "sql_err", "message": "unable to add needed audio: '.$hash.'"}');
    
    $connection->_close();
}

function update_recordings_needed($missingRecordID, $file)
{
    $sql  = "UPDATE `audio_files_needed` SET `requested` = ? WHERE `id` = ?";
    
    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);
    $stmt->bind_param("si", $file, $missingRecordID);
    
    if(!$stmt->execute())
        die('{"status": "sql_err", "message": "unable to add needed audio: '.$hash.'"}');
    
    $connection->_close();
}



function logReport($reportType, $date, $path,$name )
{
    $sql = "INSERT INTO `audio_report_exports` (`report_type`, `date_run`, `file_path`, `file_name`) VALUES(?, ?, ?, ?)";
    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);
    $stmt->bind_param("ssss", $reportType, $date, $path, $name);
       
    if(!$stmt->execute())
        die('{"status": "sql_err", "message": "unable to add needed audio: '.$hash.'"}');
    
    $connection->_close();
}

function updateHashNotes($md5, $text, $activity)
{
    $activity = "Source: ".$activity;
    $sql = "UPDATE `audio_hash` SET `audio_text` = ? AND `notes` = ? WHERE `audio_hash_id` = ?";
    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);
    $stmt->bind_param("sss", $text, $activity , $md5);

    if(!$stmt->execute())
        die('{"status": "sql_err", "message": "unable to add needed audio: '.$hash.'"}');
    
    $connection->_close();
}

function voiceProfileExists($voiceID, $language)
{
    $result = false;
    /*confirm ID does not exist*/
    $sql = "SELECT `id` FROM audio_voice WHERE `voice` = ? AND `lang` = ?";

    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);
    $stmt->bind_param("ss", $voiceID, $language);

    if(!$stmt->execute())
        die('{"status": "sql_err", "message": "unable to check voice id"}');
    
    $stmt->store_result();
    
    if($stmt->num_rows > 0)
        $result = true;
  
    $connection->_close();
    return $result;
}

function uploadNewVoiceID($voiceID, $language, $gender)
{
    $sql = "INSERT INTO `audio_voice` (`voice`, `lang`, `gender`) VALUES (?, ?, ?)";

    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);
    $stmt->bind_param("sss", $voiceID, $language, $gender);

    if(!$stmt->execute())
    {
        die('{"status": "sql_err", "message": "unable to upload voice profile"}');
    }
    $connection->_close();
}


/*menu building functions*/
function getAudioProfileList()
{
    $sql = "select `id`, `voice`, `lang`, `gender` FROM audio_voice";

    $results = [];

    $connection = new DbConn();

    $stmt = $connection->_prepare($sql);

    if(!$stmt->execute())
        die('{"status": "sql_err", "message": "unable to retrieve voice ID"}');
    
    $stmt->bind_result($id, $voice, $lang, $gender);

    while($stmt->fetch())
    {
        $entry = [];
        $entry['id'] = $id;
        $entry['voice'] = $voice;
        $entry['lang'] = $lang;
        $entry['gender'] = $gender;

        $results[] = $entry;
    }
    $connection->_close();
    return $results;
}


function getReportCategories()
{
    $sql = "SELECT DISTINCT `report_type` FROM `audio_report_exports`";
    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);
    $results = [];

    if(!$stmt->execute())
        die('{"status": "sql_err", "message": "unable to retrieve voice ID"}');

    $stmt->bind_result($type);
    while($stmt->fetch())
    {
    $results[] = $type;
    }
    $connection->_close();
    return $results;
}

function getReportFilesByType($category)
{
    $sql = "SELECT `date_run`, `file_path`, `file_name` FROM `audio_report_exports` WHERE `report_type` = ?";
    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);
    $stmt->bind_param("s", $category);
    $results = [];

    if(!$stmt->execute())
        die('{"status": "sql_err", "message": "unable to retrieve voice ID"}');

    $stmt->bind_result($date, $path, $name);
    while($stmt->fetch())
    {
        $entry['date'] = $date;
        $entry['path'] = $path;
        $entry['name'] = $name;
        $results[] = $entry;
    }
    $connection->_close();
    return $results;
}

function get_Md5_by_missing_recording_id($audio_files_needed_id)
{
    $sql = "SELECT `audio_hash_id` FROM `audio_files_needed` WHERE `id` = ?";
    $connection = new DbConn();
    $link = $connection->link();
    if(!$stmt = $link->prepare($sql))
        //die("prepare fail");
        return false;
    $stmt->bind_param("i", $audio_files_needed_id);

    

    if(!$stmt->execute())
    die("exec fail");
        //return false;

    $stmt->bind_result($id);

    
    while($stmt->fetch())
    {
        $connection->_close();
        return $id;
    }
die("past while");
    return false;
}

function  update_audio_files_needed($audio_files_needed_id, $selectedVoiceID)
{
    $sql = "UPDATE `audio_files_needed` SET `audio_voice_id_used`= ? WHERE `id` = ?";

    $connection = new DbConn();
    $link = $connection->link();
    
    if(!$stmt = $link->prepare($sql))
        return false;
    
    $stmt->bind_param("si", $selectedVoiceID, $audio_files_needed_id);

    if(!$stmt->execute())
        return false;
    
    $connection->_close();
    return true;
}


/*once audio file has been migrated to CDN, append record with a timestamp marking the upload
    this way, we can easily determine which records in `audio_files` table have been migrated. 
*/
function  update_audio_files_with_upload_timestamp($recordID)
{


    $sql = "UPDATE audio_files SET `migrated` = NOW() WHERE `id` = ?";
    $connection = new DbConn();
    $link = $connection->link();

    if(!$stmt = $link->prepare($sql))
        return false;
    
    $stmt->bind_param("i", $recordID);

    if(!$stmt->execute())
    {
        return false;
    }
    $connection->_close();
    return true;
}

function record_has_timestamp($recordID)
{
    $sql = "SELECT `audio_hash_id` FROM `audio_files` WHERE `id` = ? AND migrated IS NOT NULL";

        $connection = new DbConn();
        $link = $connection->link();

        if(!$stmt = $link->prepare($sql))
        die('{"status": "db_failure", "message": "function record_has_timestamp() prepare"}');
    
        $stmt->bind_param("i", $recordID);

        if(!$stmt->execute())
            die('{"status": "db_failure", "message": "function record_has_timestamp() execute"}');

        $stmt->store_result();

        if($stmt->num_rows > 0)
            return true;
            return false;
}


function get_audio_records_already_migrated()
{
    $sql = "SELECT `id`, `audio_hash_id`, `audio_voice_id`  FROM audio.audio_files WHERE `migrated` IS NOT NULL ";
    $connection = new DbConn();
    $link = $connection->link();

   

    if(!$stmt = $link->prepare($sql))
        die('{"status": "db_failure", "message": "function get_audio_records_already_migrated() prepare"}');
    if(!$stmt->execute())
        die('{"status": "db_failure", "message": "function get_audio_records_already_migrated() execute"}');



     $stmt->bind_result($id, $hash, $voiceID);

     $results = [];
      

     while($stmt->fetch())
     {
        $data = [];
        $data['id'] = $id;
        $data['hash'] = $hash;
        $data['voiceID'] = $voiceID;
        $results[] = $data;
     }
 
    
     $connection->_close();
     return $results;
}


function remove_timestamp_from_errant_record($recordID)
{   
    $sql = "UPDATE `audio_files` SET `migrated` = NULL where id = ?";
    $connection = new DbConn();
    $link = $connection->link();



    if(!$stmt = $link->prepare($sql))
        die('{"status": "db_failure", "message": "function get_audio_records_already_migrated() prepare"}');
    
     $stmt->bind_param("i", $recordID);
     
    if(!$stmt->execute())
        die('{"status": "db_failure", "message": "function get_audio_records_already_migrated() execute"}');
     
     $connection->_close();
}



/*utility functions*/
function md5Dir($md5)
{
    return $md5[0].$md5[1].$md5[2];
}

?>