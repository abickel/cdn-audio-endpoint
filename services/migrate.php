<?php
ini_set("display_errors", "off");
include_once("include/db.php");

$sql = "SELECT `id`, `audio_hash_id`, `audio_voice_id`, `tempUrl` FROM `audio_files` WHERE `migrated`IS NULL LIMIT 50";
    $connection = new DbConn();
    $link = $connection->link();

if(!$stmt = $link->prepare($sql))
    return false;
    
    $stmt->bind_param("i", $recordID);

    if(!$stmt->execute())
        die("unable to retrieve records");
    
       $stmt->bind_result($id, $hash, $voiceID, $url);

 $results = [];     
       while($stmt->fetch())
       {
        $entry = [];
        $entry['id'] = $id;
        $entry['hash'] = $hash;
        $entry['voice'] = $voiceID;
        $entry['url'] = $url;
        $results[] = $entry;
       }

?>

<html>

<head>

<style>
#message1
{

}
#errorMsg
{
color: red;
}
#successMsg
{
color: green
}
</style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<script>

 <?php
    echo "var records = [];";
    foreach($results as $record)
    {
        echo 'records.push({"recordID": "'.$record['id'].'", "md5": "'.$record['hash'].'", "voicdID": "'.$record['voice'].'", "url": "'.$record['url'].'"});';
    }
 ?>

    var index = 0,
        totalRecords = records.length;

    $(document).ready(function(){

             if(records.length > 0)
                {
                    $("#message1").empty();
                    migrate_record(0);
                }


        $("#migrate").on('click', function(){

                if(records.length > 0)
                {
                    $("#message1").empty();
                    migrate_record(0);
                }

        });

        $("#verify").on('click', function(){

            var payload = {"verify": "true"};
        $.post("upload.php", payload, function(response) {

            var data = JSON.parse(response);

            if(data.status)
            {
                $("#message1").empty();
            $("#message1").text("verify result: "+ data.message);
            }
            else
            {
                if(data.isArray)
                {
                $("#message1").empty();
                $("#message1").text("verify result: "+ data.length + " records found to be errant. Datasbase records have been updated to migration need.");
                }
            
            }
   
	  });

        });
    });
    
function migrate_record(index)
{
   // console.log(index);

    if(index < totalRecords)
    {
        var obj = records[index];
        var payload = {"arrayIndex": index, 
                        "md5": obj.md5, 
                        "voice_id": obj.voicdID, 
                        "source_url": obj.url, 
                        "override": "false", 
                        "recordID": obj.recordID,
                        "migrate": true};
                       //console.log(payload);
        
        $.ajax({
            url: "upload.php",
            type: "post",
            data: payload,
            success: function(response){

               // console.log(response);

                var data = JSON.parse(response);
                var nextIndex = data.index + 1;

                if(data.status == "error")
                {
                   $("#errorMsg").append('<p>error: '+data.state+', row ID: '+ data.index +', message: '+data.message +'m source: '+data.source+'</p>');  
                   window.location.reload(true);
                }
                else if (data.status == "exists")
                {
                    $("#errorMsg").append('<p>duplicate record id '+ data.recordID+',  url: '+ data.cdnUrl +'</p>');
                }

                //$("#message1").empty();
                $("#message1").text((index +1) + " of " + totalRecords + " processed.");

                migrate_record((index + 1)); //next record
            },

            error: function(response){

                $("#errorMsg").append('<p>error: '+ response +'</p>');
                window.location.reload(true);
            }
        });
    }
    else
    {
        //$("#message1").empty();
        $("#message1").text("complete!   "+ index + " of " + totalRecords + " processed.");
        window.location.reload(true);
    }
  
}
</script>

</head>


<body>


<div>
Migrate >>> <button id="migrate">Do It</button>
</div> 

<div>
Verify >>> <button id="verify">Do It</button>
</div> 

<div id="message1">
</div>

<div id="errorMsg">
</div>

<div id="successMsg">
</div>
</body>


</html>