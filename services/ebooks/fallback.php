<?php
include("../include/db.php");
include("../include/db_functions.php");
    $md5 = filter_input(INPUT_POST, 'md5', FILTER_SANITIZE_STRING);              // hash to find fallback for
    $vid = filter_input(INPUT_POST, 'lang_voice_id', FILTER_SANITIZE_STRING); 

$sql = "CALL audio.getTextFallbacks('".$md5."')";    

$connection = new DbConn();
$mysqliconnect = $connection->link();
$result = $mysqliconnect->query($sql);


if($result)
{
    if($result->num_rows > 0)
    {
        $rval = [];
        $rval["status"] = "result_found";
        $rval["content"] = [];

        while($row = mysqli_fetch_array($result))
        {
            $item = [];

            $id_array = getValidVoiceIdForHash($row['hash'], $vid);

            if(count($id_array) > 0)        //do we have voice ID in proper language for this potential substitute?
            {
            $item['hash'] = $row['hash'];
            $item['text'] = $row['text'];
            $item['notes'] = $row['notes'];
            $item['vid_arr'] = $id_array;
            $rval["content"][] = $item;
            }

        }

        echo json_encode($rval, true);
        exit();
    }
    echo '{"error": "empty_set"}';
    exit();
}
    echo '{"error": "query_failure"}';
    exit();


function getValidVoiceIdForHash($md5, $vid)
{
    $results = [];

    $sql = "SELECT DISTINCT `audio_voice_id` FROM `audio_files` WHERE audio_hash_id = '".$md5."'";
    $connection = new DbConn();
    $mysqliconnect = $connection->link();

    $result = $mysqliconnect->query($sql);

    if($result)
    {
        while($row = mysqli_fetch_array($result))
        {
            if(languagMatches($vid, $row['audio_voice_id']))
            {
                $results[] = $row['audio_voice_id'];
            }
        }

    }
    return $results;
}


function languagMatches($id1, $id2)
{
    $sql = "Call language_matches(".$id1.",".$id2.")";

    $connection = new DbConn();

    $mysqliconnect = $connection->link();

    $result = $mysqliconnect->query($sql);

    if($result)
    {
        while($row = mysqli_fetch_array($result))
        {
            $res = $row['rval'];

            if($res === "true")
                return true;
        }
    }
    return false;
}
?>
