<?php
/* SFTP */
$program = $_POST['program'];
$book = $_POST['book'];
$start = $_POST['start'];
$end = $_POST['end'];
$languageArray = $_POST['languageArray'];
$page = "";



if ((isset($program)) && (isset($book)) && (isset($start)) && (isset($end)) && (isset($languageArray))) {

  
    while ($start < $end) {

        $pageNum = "";
        //Integrated had an extra zero in the padding (4 instead of 3)!!!!!
        if ($program == "dcs_cchsim") {

            $pageNum = "page" . sprintf("%'04s", $start) . ".xhtml";
        } else {
            $pageNum = "page" . sprintf("%'03s", $start) . ".xhtml";
        }

        $contents = file_get_contents('../../epub_extracts/' . $program . '/' . $book . "/ops/" . $pageNum);


        $bodystart = strpos($contents, "<body");
        $bodyend = strpos($contents, "</body>");
        $bodylength = $bodyend - $bodystart;


        $contents = substr($contents, $bodystart, $bodylength);

        $page .= stupify($contents, $program, $book);

        $page = killEmbeddedAnchors($page);

        $start++;
    }
    echo "<head></head><body>";
    echo $page;
}





/* jklins magic */

function stupify($str, $program, $book) {
    return str_replace('js/glossary', $program . '/' . $book . '/OPS/js/glossary', str_replace('glossary/exam', $program . '/' . $book . '/OPS/glossary/exam', str_replace('glossary/jqu', $program . '/' . $book . '/OPS/glossary/jqu', str_replace('js/nifty', $program . '/' . $book . '/OPS/js/nifty', str_replace('js/dc', $program . '/' . $book . '/OPS/js/dc', str_replace('js/jquery', $program . '/' . $book . '/OPS/js/bjquery', str_replace('href="css/', 'x="', str_replace('<img', '<stupid', $str))))))));
}

function embedPageNumber($str) {
    
}

function killEmbeddedAnchors($page) {

    //have to remove popup_word links at this level, otherwise the dom will correctly fix the disallowed nested anchors and mess up what we thinkg we should be hasing...
    $foundLink = true;
    while ($foundLink) {
        $startPoint = strpos($page, "popup_word");
        if ($startPoint > 0) {
            //find the anchor start by reverse string search of the page ending at our startpoint.
            $startPoint = strrpos(substr($page, 0, $startPoint), "<a");
            //find where the anchor tags text starts
            $textStartPoint = $startPoint + strpos(substr($page, $startPoint), ">") + 1;
            //find where the anchor tags text ends
            $endPoint = $startPoint + strpos(substr($page, $startPoint), "</a>");
            //the string we want read is the substr of the page from the textStartPoint, the number of characters is the endPoint - textStartPoint
            $text = substr($page, $textStartPoint, $endPoint - $textStartPoint);
            //ok so piece the page back together removing the offending anchor tag for the popup_word and making sure to remove the ending anchor tag
            $page = substr($page, 0, $startPoint) . substr($page, $textStartPoint, $endPoint - $textStartPoint) . substr($page, $textStartPoint + $endPoint - $textStartPoint + 4);
        } else {
            $foundLink = false;
        }
    }
    return $page;
}
?>

<script src="./js/utilities.js"></script>
<script>

    /*this function triggers upon return to client page.*/
    $(function () {
        
                /*block while parsing fallback data*/
        $.blockUI({
            message: $("#overviewMsg")
        });
        
        var result = [];
        var mapped = [];

        /*loop through elements with parsable audio elements*/
        $.each($('a[href^=javascript\\:play_audio]'), function () {
            var item = {};
            var links = [];

            var str = $(this).context.href;
            var hash = getFileHash($(this).text()).str;
            var identifier = $(this).find(":first-child").attr("id");

            item.str = $(this).text();
            item.hash = hash;
            item.voices = []
            item.links = [];
            /*loop through each voice ID passed from client*/
<?php            foreach($languageArray as $voiceID)    {       ?>

            item.voices.push("<?php echo $voiceID ?>");

            item.links.push(hash[0] + hash[1] + hash[2] + "/<?php echo $voiceID ?>/" + hash + ".mp3"); 
            item.identifier = identifier;

<?php            }            ?>



            if ($(this).text().length > 1)
            {
                if (mapped.indexOf(item.hash) == -1)
                {
                    mapped.push(item.hash);
                    result.push(item);
                }
            }


            //$(this).context.href = "javascript:play_audio_hash('" + fileInfo + "')";
            // $(this).addClass('oldLink-' + str.replace('javascript:play_audio(', '').replace(')', '').replace(/['";]+/g, ''));

        });
        $(".resource-library").empty();
        writeData(result);


        /*link from fallback audio playback*/

        $(document).on('click', '.audio_link', function (e) {
            e.preventDefault();
            var link = $(this).attr("href");
            var src = document.createElement("source");
            var player = $("#audio_player");

            src.src = link;

            player[0].pause();
            player.empty();
            player.append(src);
            player[0].load();
            player.trigger("play");
        });
    });

    function writeData(result)
    {
        /*establish voice id array*/

        var target = $(".resource-library");

        var tbl = $("<table id='overviewTable'></table>");
        target.append(tbl);

        var th = $('<tr id="fallback-overview-header" style="text-align: center;" ></tr>');
        th.append('<th style="text-align: center;">Hash</th>');
        th.append('<th style="text-align: center;">String</th>');
        th.append('<th style="text-align: center;">Page</th>');
        th.append('<th style="text-align: center;">Voice Id</th>');
        th.append('<th style="text-align: center;">Fallback</th>');

        var thead = $("<thead></thead>");
            thead.append(th);

        tbl.append(thead);
        tbl.append("<tbody>");


        var ttl_rows = (result.length - 1);

        
        for (var i in result)
        {
            var row = result[i];

                row.row_idx = i;

            var pageNum = parsePageNumber(row.identifier);

            /*build the table structure*/
            var tr = $('<tr style="border: 1px solid black"></tr>');
            tr.append('<td style="width: 22%; font-size: 12px margin-top: 10px; text-align: center">' + row.hash + '</td>');
            tr.append('<td style="width: 22%; font-size: 12px margin-top: 10px; text-align: center">' + row.str + '</td>');
            tr.append('<td style="width: 22%; font-size: 12px margin-top: 10px; text-align: center">' + pageNum + '</td>');
            tr.append('<td style="width: 12%; font-size: 12px margin-top: 10px; text-align: center" id="cdn_voice_' + row.hash + '"></td>');
            tr.append('<td style="width: 22%; font-size: 12px margin-top: 10px; text-align: center" id="cdn_link_' + row.hash + '"></td>');
            tbl.append(tr);

            checkAudioRecordingExists(row, 0, ttl_rows);       //look for proper audio


        }
         tbl.append("</tbody>");
    }


/*obj {'hash': md5, 'identifier': page No., "voices": array of voice ids, "links": lisdt of urls, "str": text of hash, 'row_idx': position of this object within the ttl collection}*/
/*beginningVoiceIndex :  position within voice array that we're seeking.  increment with each call to checkAudio*/
/*totalRows: total number of results in the set for this chapter, use this to calculate and animate progress to user (whole chapter's processing can take 10-20 secs.)*/

    function checkAudioRecordingExists(obj, beginningIndex, totalRows)
    {
        var endPoint = obj.voices.length;



        if(beginningIndex < endPoint)      //do we still have IDs to check?
        {
            var baseurl = "http://68b9c1bc7c9fa35e4a6d-0264cef6aad1b81efe617b95ad9e9064.r0.cf2.rackcdn.com";
            var cdnPath = obj.links[beginningIndex];

            var address = baseurl + "/" + obj.links[beginningIndex];

            $.ajax({ url: address,
                type: 'head',
                md5: obj.hash,
                lang: obj.voices[beginningIndex],
                href: address,
                idx: obj.row_idx,
                text: obj.str,
                total: totalRows,
                success: function(response){

                    build_Anchor(this.md5, this.href);
                    showVoiceID(this.lang, this.md5);
                    showProgress(this.idx, this.total, );

                    if(this.idx == this.total)
                        releaseClient();
                },
                error: function(response){      //no dice, check the next index

                    checkAudioRecordingExists(obj, (beginningIndex + 1), totalRows);

                }
            });

        }
        else            //no more to check, we've exhausted our Id array
        {
            /*build the list of id's as a string to show client what we DO have available*/
            var id_TD  = $("#cdn_voice_" + obj.hash ),
                btn_TD = ("#cdn_link_" + obj.hash),
                id_string = "";


            for (var j in obj.voices) {
                id_string += obj.voices[j];
                
                if (j != (obj.voices.length - 1)) {
                    id_string += ", ";
                }
            }
            id_TD.text("checked: " + id_string);      //display the list of voice id's tried for this hash

            //give us a button to search for fallback options for this hash
            build_fallback(obj.hash,  obj.voices, obj.str);


            /*last item in the list, and we've exhausted its voice array - return control to client now.*/
            if(obj.row_idx ==  totalRows)
            {
                releaseClient();
            }
        }
    }
    

    function showProgress(obj_width, ttl_width)
    {
        var perc = Math.floor((obj_width /ttl_width ) * 100);
        var parentwidth = $("#progress-container").width();
        var barWidth = $("#load-progress").width();
        var curPerc = Math.floor((barWidth / parentwidth ) * 100);
        
                
         if((perc > curPerc) && ($("#load-progress").data("active") == "true"))     //if new percentage is larger, update the bar
            $("#load-progress").css("width", perc+"%");


    }


    

    function releaseClient()
    {
        $.unblockUI();
        $("#load-progress").data("active", "false");
        $('#overviewTable').DataTable({"paging":   false});
    }

    function build_Anchor(hash, link) {

        if (!($("#cdn_link_" + hash).data("set") === "true"))
        {
            var content = $('<a class="audio_link" href="' + link + '">Play Default Audio</a>');
            $("#cdn_link_" + hash).append(content);
            $("#cdn_link_" + hash).data("set", "true");
            $("#cdn_link_" + hash).data("md5", hash);
        }
    }

    function showVoiceID(voiceID, audio_hash)
    {
        id_TD  = $("#cdn_voice_" + audio_hash );
        id_TD.text(voiceID);
    }

    /*default audio file not found, so provide the option to search for falbacks*/
    function build_fallback(hash,  voices, text) {

        if (!($("#cdn_link_" + hash).data("set") === "true"))
        {   
       
            $("#cdn_link_" + hash).append('<button class="fallback-button">View Fallback Options</button>');
            $("#cdn_link_" + hash).data("set", "true");
            $("#cdn_link_" + hash).data("md5", hash);
            $("#cdn_link_" + hash).data("text", text);
            $("#cdn_link_" + hash).data("voiceArray", voices);

        }
    }



    function parsePageNumber(str) {

        var begin = str.indexOf("P") + 1;
        var end = str.indexOf("_");
        var length = end - begin;

        var pageNumber = str.substr(begin, length);
        return pageNumber + ".xhtml";
    }


</script>
</body>
