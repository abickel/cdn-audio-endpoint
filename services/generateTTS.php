<?php
ini_set("display_errors", "on");
include_once("include/db.php");
include_once("include/db_functions.php");
require '../aws/aws-autoloader.php';
include_once("include/api_credentials.php");




/*$_POST vars*/
$text = filter_input(INPUT_POST, 'audio-string', FILTER_SANITIZE_STRING);
$filename = filter_input(INPUT_POST, 'file-name', FILTER_SANITIZE_STRING);
$language = filter_input(INPUT_POST, 'language', FILTER_SANITIZE_STRING);
$uploadAudio = $_POST['create-recording'];


if(isset($uploadAudio))
{
    /******************
AWS Polly API init
*******************/
$credentials    = new \Aws\Credentials\Credentials($awsAccessKeyId, $awsSecretKey);
$client         = new \Aws\Polly\PollyClient([
    'version'     => 'latest',
    'credentials' => $credentials,
    'region'      => 'us-west-2'
]);


$polly_voice_id = (strtolower($language) === "e")? "Joanna" : "Penelope";


/*need polly*/
$url =  $client->createSynthesizeSpeechPreSignedUrl([
                
    'OutputFormat' => 'mp3',
    'Text'         => utf8_encode($text),
    'VoiceId'      => $polly_voice_id,
]);

/*config stream https*/
$arrContextOptions=array(
"ssl"=>array(
    "verify_peer"=>false,
    "verify_peer_name"=>false,
    ),
);

/*retrieve file*/
$response = file_get_contents($url, false, stream_context_create($arrContextOptions)); 


/*save*/
if(!file_exists("../exports/generatedTTS"))
{
    if(!mkdir("../exports/generatedTTS"))
    {
        header( 'Location: ..create-tts-recording.php?status=write_error&msg=unable_to_mk_dir') ;
        exit();
    }
}
 if(file_put_contents("../exports/generatedTTS/".$filename.".mp3", $response))
 {
    header( 'Location: ../create-tts-recording.php?status=success&msg='.$filename) ;
    exit();
 }
        header( 'Location: ../create-tts-recording.php?status=write_error&msg=unable_to_save'.$filename) ;
        exit();
}

?>
