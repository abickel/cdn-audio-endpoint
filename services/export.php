<?php
include("include/db.php");
include("include/db_functions.php");
ini_set("display_errors", "on");

$reportType = (filter_input(INPUT_POST, 'rtype', FILTER_SANITIZE_STRING));



switch($reportType)

{
    /*retrieve records that have not already been requested*/
    case  "all_missing":
    {
        $sql = "SELECT  src.audio_hash_id AS md5,"; 
		$sql.= "src.audio_text AS speechText,";
        $sql.= "src.notes AS notes,";
        $sql.= "needed.preferred_audio_voice_id AS voiceID,";
        $sql.= "CONCAT(needed.id , '.mp3') AS fileName,";
        $sql.= "needed.id  AS missing_record_id,";
        $sql.= "vp.voice,";
        $sql.= "vp.lang,";
        $sql.= "vp.gender ";
        $sql.= "FROM audio.audio_hash AS src ";
        $sql.= "JOIN audio.audio_files_needed AS needed ";
        $sql.= "ON src.audio_hash_id = needed.audio_hash_id AND needed.requested = '0'";
        $sql.= "JOIN audio.audio_voice AS vp ON needed.preferred_audio_voice_id = vp.id ";

        $connection = new DbConn();
        $stmt = $connection->_prepare($sql);

        if(!$stmt->execute())
        {
            die('{"status": "sql_err", "message": "unable to export missing audio listing"}');
        }

        $stmt->bind_result($hash, $text, $notes, $voiceID, $recordingfileName, $missingRecordID, $voiceName, $language, $gender);

        $dateobj = new DateTime();

        $date = $dateobj->format("Y-m-d");
        $timeStamp  = $dateobj->format('Y-m-d h:i:s');
        $relative_path = "../exports/missing";
        $absoloutePath = "exports/missing";
        $exportfileName = "audio_needed_".$dateobj->format('Y-m-d_his').".csv";
        
        $file = $relative_path."/".$exportfileName;

        if (!file_exists($relative_path)) {
            if (!mkdir($relative_path, 0775, true)) {
                die( '{"status": "error", "message":"unable to create directory: ' . $relative_path . '"}');
          
            }
        }

        $handle = fopen($file, "w");
        $header  = "HASH, TEXT, FILENAME, VOICE, LANGUAGE, GENDER, VOICE ID, NOTES\n\r";
        fwrite($handle, $header);
        $numRows = 0;
        while($stmt->fetch())
        {
            $content = array();
            $content[] = $hash;
            $content[] = $text;
            $content[] = $recordingfileName;
            $content[] = $voiceName;
            $content[] = $language;
            $content[] = $gender;
            $content[] = $voiceID;
            $content[] = $notes;

            $str = $hash.", ".$text.", ".$recordingfileName.", ".$voiceName.", ".$language.", ".$gender.", ".$voiceID.", ".$notes.", \n\r";
            fputcsv($handle, $content);

            /*update record, mark which file is requesting the records*/
            update_recordings_needed($missingRecordID, $exportfileName);

            $numRows++;
        }

        fclose($handle);

        if($numRows > 0)
        {
        /*need to update the 'reports available' table*/
        logReport("missing", $timeStamp, $absoloutePath, $exportfileName);
        echo '{"status": "success", "type": "missing",  "file": "'.$exportfileName.'"}';
        exit();
        }

        else    /*dont keep an empty file*/
        {
            unlink($file);
             echo '{"status": "no_records", "type": "missing",  "file": "'.$exportfileName.'"}';
        }
    }
}


?>