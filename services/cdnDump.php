<?php
require '../vendor/autoload.php';
include_once("include/api_credentials.php");

use OpenCloud\Rackspace;

ini_set("display_errors", "on");
$client = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array(
    'username' => $rackspace_user,
    'apiKey' => $rackspace_api_key
));

$objectStoreService = $client->objectStoreService(null, 'ORD');
$audioContainer = $objectStoreService->getContainer('big_ideas_math_audio');

if ( $_SERVER['REQUEST_METHOD'] == 'POST' )
{
if(isset($_POST['delete-file']))
{
    $fileName = $_POST['file'];
    if($object = $audioContainer->objectExists($fileName))
    {
        $dfile = $audioContainer->getObject($fileName);
        $audioContainer->DeleteObject($fileName);
    }

}
}

$account = $objectStoreService->getAccount();
$account->setTempUrlSecret();



$files = $audioContainer->objectList();


?>
<form enctype="multipart/form-data" action="cdnDump.php" method="post">
<div>purge: <input type="text" name="file"><input type="submit" name="delete-file"></div>
</form>
<?php

foreach ($files as $file) {

        $exists = $audioContainer->objectExists($file->getName());

        if($exists)
            //echo "<a href".$file->getName()." <br>location: ".$file->getPublicUrl()."<br><br>";
            echo '<a href="'.$file->getPublicUrl().'">'.$file->getName().'</a><br>';
}

?>