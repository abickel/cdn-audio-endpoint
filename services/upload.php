<?php
ini_set("display_errors", "off");
include_once("include/db.php");
include_once("include/db_functions.php");
include_once("include/api_credentials.php");
require '../aws/aws-autoloader.php';
require '../vendor/autoload.php';
use OpenCloud\Rackspace;


/*$_POST vars*/
$uploadAudioID = filter_input(INPUT_POST, 'uaID', FILTER_SANITIZE_STRING);
$getAudioIDs = filter_input(INPUT_POST, 'getAudioIDs', FILTER_SANITIZE_STRING);
$migrate = filter_input(INPUT_POST, 'migrate', FILTER_SANITIZE_STRING);
$uploadAudio = $_POST['upload_audio_button'];


/*upload new voice profile to db*/
if(isset($uploadAudioID))
{
    $voiceID = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    $language = filter_input(INPUT_POST, 'language', FILTER_SANITIZE_STRING);
    $gender = filter_input(INPUT_POST, 'gender', FILTER_SANITIZE_STRING);

    if(voiceProfileExists($voiceID, $language))
    {
        die('{"status": "dup_err", "message": "Voice ID exists."}');
    }
    uploadNewVoiceID($voiceID, $language, $gender);
    die('{"status": "success", "message": "Voice ID added."}');
}

if(isset($uploadAudio))
{
     $selectedVoiceID  = filter_input(INPUT_POST, 'voice_id_options', FILTER_SANITIZE_STRING);
     $allowOverride  = filter_input(INPUT_POST, 'allow_override', FILTER_SANITIZE_STRING) === 'true'? true: false;


    if(isset($_FILES))
    {
        $files = reArrayFiles($_FILES['target_file']);

     
        foreach($files as $file)
        {
            $name = $file['name'];
            $audio_files_needed_id = str_replace(".mp3", "", $name);
            $type = $file['type'];
            $temp_location = $file['tmp_name'];
            $error = $file['error'];
                
            if(($type === "audio/mp3")&&($error === 0))
            {

                /*retrieve md5 of this record's ID*/
                    if(!$md5 = get_Md5_by_missing_recording_id($audio_files_needed_id))
                    {
                        header( 'Location: ../index.php?status=sql_error&msg='.$name."&id=".$audio_files_needed_id);
                        exit();
                    }

                /*we have a valid record associated with this file, now uppload*/
                /************************ rackspace API initialization *************************/
                //https://developer.rackspace.com/docs/cloud-files/quickstart/?lang=php
                //http://php-opencloud.readthedocs.io/en/latest/services/object-store/objects.html?highlight=container
                $rsClient = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array('username' => $rackspace_user,'apiKey' => $rackspace_api_key )); 


                $objectStoreService = $rsClient->objectStoreService(null, 'ORD');
                $audioContainer = $objectStoreService->getContainer('big_ideas_math_audio');

                $baseDir = md5Dir($md5);
                $cdnFileName = $baseDir."/".$selectedVoiceID."/".$md5.".mp3";

                /*if no overwrite permission*/
                if(!$allowOverride)
                {
                    /*and file exists*/
                if($audioContainer->objectExists($cdnFileName))
                    {
                        /*exit*/
                        header( 'Location: ../index.php?status=duplicate_error&msg='.$name ) ;
                        exit();
                    }
                }
                else
                {

            
                        /*we have overwrite permission and the file exists*/
                    if($audioContainer->objectExists($cdnFileName))
                    {
               
                        $obj = $audioContainer->getObject($cdnFileName);

                        $obj->purge();          //flush CDN

                        $audioContainer->DeleteObject($cdnFileName);

                    }
                }
                /*file does not exist, attempt to upload*/    
                try{
                    
                    $custom_headers = array('Access-Control-Allow-Origin' => '*');

                    $tmpFile = file_get_contents($temp_location);

                    $audioContainer->uploadObject($cdnFileName, $tmpFile, $custom_headers);

                    /*file uploaded successfully, update the audio_files_needed record*/
                    
                    update_audio_files_needed($audio_files_needed_id, $selectedVoiceID);
                    header( 'Location: ../index.php?status=success&msg=cdnLocation'.$cdnFileName);
                    exit();


                } catch(Exception $e)
                {
                    header( 'Location: ../index.php?status=err&msg=cdn_upload_error');
                    exit();
                }
            }
            else{
                header( 'Location: ../index.php?status=upload_error&msg='.$name);
                exit();
            }
        }
    }
}


/*check that uploaded records are actually in the cdn*/
if(isset($_POST['verify']))
{

   $results = get_audio_records_already_migrated();

  
    if(count($results > 0))
    {
    $rsClient = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array('username' => $rackspace_user,'apiKey' => $rackspace_api_key )); 

                $objectStoreService = $rsClient->objectStoreService(null, 'ORD');
                $audioContainer = $objectStoreService->getContainer('big_ideas_math_audio');


        $dispar = [];

        foreach($results as $record)
        {
            $shortcode = md5Dir($record['hash']);
            $voiceID = $record['voiceID'];

            $cdnFileName = $shortcode."/".$voiceID."/".$record['hash'].".mp3";

            /*no audio file even though DB thinks we've uploaded one*/
            if(!$audioContainer->objectExists($cdnFileName))
            {
                $dispar[] = $record['id'];

                /*reset timestamp to 'NULL' in db so next migrate pass catches it.*/
                remove_timestamp_from_errant_record($record['id']);
            }
        }
        if(count($dispar) > 0)
        {

             echo json_encode($dispar);
        }
        else{
            echo '{"status": "success", "message": "no errant records found"}';
        }
    }
    else
    {
        die('{"status": "db_failure", "message": "unable to retrieve migrarted audio records"}');
    }
}


/*utility*/
/* http://php.net/manual/en/features.file-upload.multiple.php  */
function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}
?>