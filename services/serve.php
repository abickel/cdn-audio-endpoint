<?php
include("include/db.php");
include("include/db_functions.php");
include_once("include/api_credentials.php");
require '../aws/aws-autoloader.php';
require '../vendor/autoload.php';
use OpenCloud\Rackspace;

ini_set("display_errors", "off");
/**/

/************************rackspace API initialization*************************/
$rsClient = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array('username' => $rackspace_user,'apiKey' => $rackspace_api_key )); 
$objectStoreService = $rsClient->objectStoreService(null, 'ORD');
$audioContainer = $objectStoreService->getContainer('big_ideas_math_audio');
$audioContainer->enableCdn();           //make publicly available

/*allw for temp url from rackspace (for new polly recordings only)*/
$account = $objectStoreService->getAccount();
$account->setTempUrlSecret();



/******************AWS Polly API init*******************/
$credentials    = new \Aws\Credentials\Credentials($awsAccessKeyId, $awsSecretKey);
$pollyClient         = new \Aws\Polly\PollyClient([    
    'version' => 'latest',    
    'credentials' => $credentials,    
    'region' => 'us-west-2'
]);


/*$status vars*/
$getAudioIDs = filter_input(INPUT_POST, 'getAudioID', FILTER_SANITIZE_STRING);              //client requests list of available voice ID's to create select box options
$assoc = filter_input(INPUT_POST, 'assoc', FILTER_SANITIZE_STRING);                         // are we looking for flags to color code items?
$link  = filter_input(INPUT_POST, 'link', FILTER_SANITIZE_STRING);                          //are we requesting an audio link
$reportList = filter_input(INPUT_POST, 'reportList', FILTER_SANITIZE_STRING);               //client requests listing of reports
$onlyPolly = filter_input(INPUT_POST, 'cdnFail', FILTER_SANITIZE_STRING);                   //if outside service has already failed to find resource on cdn.
$post_missing_id  = filter_input(INPUT_POST, 'post_missing_id', FILTER_SANITIZE_STRING);    //client has received a request for a voice id/recording combo that does not exist, so lets lof it to db
$getFallbackCandidateRecords = filter_input(INPUT_POST, 'query', FILTER_SANITIZE_STRING);

/*variable values*/
$language = trim(filter_input(INPUT_POST, 'language', FILTER_SANITIZE_STRING));             //get selected language


/**********************************************************/
/*        MUTUALLY EXCULSIVE STATES / ACTIONS             */
/**********************************************************/


/*client request for all records in audio_files_needed that are candidates for fallback assignment*/
if((isset($getFallbackCandidateRecords))&&($getFallbackCandidateRecords ==="needsFallback"))
{

    $sql = "SELECT * FROM eligible_for_substitute";
    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);

    if(!$stmt->execute())
        die('{"status": "sql_err", "message": "unable to retrieve fallback candidates"}');
    
    $stmt->bind_result($id, $hash, $string, $notes, $requestStatus);

    $results = [];

    while($stmt->fetch())
    {
        $record = [];
        $record['id'] = $id;
        $record['hash'] = $hash;
        $record['string'] = $string;
        $record['notes'] = $notes;
        $results [] = $record;
    }

    echo '{"status": "success", "results": '.json_encode($results).'}';
    exit();
} 


/*client request for available fallbacks for a given md5*/
if((isset($getFallbackCandidateRecords))&&($getFallbackCandidateRecords ==="getCandidateSolutions"))
{
    $md5 = filter_input(INPUT_POST, 'md5', FILTER_SANITIZE_STRING);
    $sql = 'CALL getTextFallbacks("'.$md5.'")';

    $connection = new DbConn();
    $stmt = $connection->_prepare($sql);

     if(!$stmt->execute())
        die('{"status": "sql_err", "message": "unable to retrieve fallback candidates"}');

    $stmt->bind_result($hash, $string, $notes);

    $results = [];
    
    while($stmt->fetch())
    {
        $record = [];
        $record['hash'] = $hash;
        $record['string'] = $string;
        $record['notes'] = $notes;
        $results [] = $record;
    }
     echo '{"status": "success", "results": '.json_encode($results).'}';
     exit();
}





if(isset($post_missing_id))
{
$md5 = filter_input(INPUT_POST, 'md5', FILTER_SANITIZE_STRING);             
$text = filter_input(INPUT_POST, 'text', FILTER_SANITIZE_STRING);                        
$activity  = filter_input(INPUT_POST, 'activity', FILTER_SANITIZE_STRING);                          
$voiceID = filter_input(INPUT_POST, 'voiceID', FILTER_SANITIZE_STRING);       

        /*add hash/text to table audio_hash*/
        addHashTextToDB($md5, $text, $activity);
        
        /*log required audio hash to audio_files_needed*/
        addNeededAudioFileToLog($md5,$voiceID);        
}


/*client requests list of available report files*/
if(isset($reportList))
{
    $reportCategories = getReportCategories();
    $results = [];
     $catArray = [];

    foreach( $reportCategories as $category)
    {
        $reports = getReportFilesByType($category);

        foreach($reports as $report)
		{
            $catArray[$category][] = $report;
		}
         $results[] = $catArray;
    }
        echo json_encode($results);
        exit();
}

/*client requests a specific audio file from an array of desired voice IDs*/
if(isset($link))
{
    $hash = filter_input(INPUT_POST, 'md5', FILTER_SANITIZE_STRING);    
    $voiceID = $_POST['idArray'];       //list of preferred voice IDs to search for
    $activity = filter_input(INPUT_POST, 'activity', FILTER_SANITIZE_STRING);     //which activity is this file associated with?
    $string = trim(filter_input(INPUT_POST, 'string', FILTER_SANITIZE_STRING));

    $found = false;             //flag for searching CDN
    $idx = 0;               //count index
    $fileName = "";         //name of desired file

    /*set voice id for Polly*/
    $polly_voice_id = ($language === 'e')? "Joanna": "Penelope";

    /*search the list of desired voice IDs, see if we have a match in the CDN*/
    do
    {
        $path = md5Dir($hash).'/'.$voiceID[$idx];       //get path
        $fileName = $path.'/'.$hash.'.mp3';

        $found = $audioContainer->objectExists($fileName);
        $idx++;
    } 
    while(($idx < count($voiceID))&&(!$found));

    /*recording type association*/
    $assoc = "recorded";

    if(!$found)
    {
        $assoc = "tts";

        /*add hash/text to table audio_hash*/
        addHashTextToDB($hash, $string, $activity);
        
        /*log required audio hash to audio_files_needed*/
        addNeededAudioFileToLog($hash,$voiceID[0]);

        $path = md5Dir($hash).'/tts/'.$language;       //get tts path
        $fileName = $path.'/'.$hash.'.mp3';

        /*check for saved tts*/
        if(!$audioContainer->objectExists($fileName))
        {
            /*retrieve file*/
            $response = polly_create_recording($hash, $language, $string, $polly_voice_id, $pollyClient); 

            /*upload to rackspace CDN*/
            push_to_cdn_return_url_to_client($fileName, $response, $audioContainer, $assoc);          
        }
    }

    /*return a valid url to client*/
    $object = $audioContainer->getObject($fileName);
    $cdnUrl = $object->getPublicUrl();
    echo '{"status": "found", "filename":"'.$cdnUrl.'", "recordingType": "'.$assoc.'"}';
    exit();
}


/*populate list of available voice ID's*/
else if(isset($getAudioIDs))
{
    $results = getAudioProfileList();
    echo json_encode($results);
    exit();
}

/*search cdn for associated file types for each value in the passed array*/
if(isset($assoc))
{
    $list = json_decode($_POST['md5'], true);       //expects ['md5'] as json encoded array  {'md5': '43ro7h...', "text": "quick brown foxes and such"}
    $voiceID = json_decode($_POST['idArray']);

    /*does client want to view associations only, or update the records in the database as well?*/
    $updateRecords = ($_POST['mode'] === "view")? false: true;
 
    $results  = [];   

    foreach($list as $entry)
    {
        $md5 = $entry["md5"];
        $text = $entry["text"];
        $activity =$entry["activity"];
        $found = false;           
        $idx = 0;               
        $fileName = "";       
        $result = '{"status": "parsed", "md5":"'.$md5.'", "recordingType": "recorded"}';        

            /*search the list of desired voice IDs, see if we have a match in the CDN*/
        do
        {
            $path = md5Dir($md5).'/'.$voiceID[$idx];       //get path
            $fileName = $path.'/'.$md5.'.mp3';
            $found = $audioContainer->objectExists($fileName);
            $idx++;
        }    
        while(($idx < count($voiceID))&&(!$found));

        if(!$found)
        {
            $result = '{"status": "parsed", "md5":"'.$md5.'", "recordingType": "tts"}';

                
            if($updateRecords)
            {

                addHashTextToDB($md5, $text, "Learnosity Activity-".$activity);/*add hash/text to table audio_hash*/
                addNeededAudioFileToLog($md5,$voiceID[0]);/*log required audio hash to  audio_files_needed*/
            }

            /*check for tts*/
            $path = md5Dir($md5).'/tts/'.$language;       
            $fileName = $path.'/'.$md5.'.mp3';

            $found = $audioContainer->objectExists($fileName);      

            if(!$found)
            {
                $result = '{"status": "parsed", "md5":"'.$md5.'", "recordingType": "none"}';        
            }
        }
        $results[] = $result;
    }
    echo json_encode($results);
    exit();
}

    
/*client has exhausted CDN, need AWS polly generated*/
if(isset($onlyPolly))
{
    $hash = filter_input(INPUT_POST, 'md5', FILTER_SANITIZE_STRING);    
    $language = filter_input(INPUT_POST, 'language', FILTER_SANITIZE_STRING); 
    $voiceID = filter_input(INPUT_POST, 'voiceID', FILTER_SANITIZE_STRING);
    $activity = filter_input(INPUT_POST, 'activity', FILTER_SANITIZE_STRING);     //which activity is this file associated with?
    $string = trim(filter_input(INPUT_POST, 'string', FILTER_SANITIZE_STRING));

    $fileName = md5Dir($hash).'/tts/'.$language."/".$hash.".mp3";

    $polly_voice_id = ($language === 'e')? "Joanna": "Penelope";

    /*add hash/text to table audio_hash*/
    addHashTextToDB($hash, $string, $activity);
        
    /*log required audio hash to audio_files_needed*/
    addNeededAudioFileToLog($hash,$voiceID);

    /*retrieve file*/
    $response = polly_create_recording($hash, $language, $string, $polly_voice_id, $pollyClient); 

    /*upload to rackspace CDN*/
    push_to_cdn_return_url_to_client($fileName, $response, $audioContainer, "tts");     
}



/*************FUNCTION DEFINITIONS BELOW***************/

function polly_create_recording($md5, $language, $text, $voiceID, &$pollyClient)
{
    $url =  $pollyClient->createSynthesizeSpeechPreSignedUrl([
        'OutputFormat' => 'mp3',
        'Text'         => $text,
        'VoiceId'      => $voiceID,
    ]);

    /*config stream https*/
    $arrContextOptions = array(
        "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
         ),
    );
    /*retrieve file*/
    return file_get_contents($url, false, stream_context_create($arrContextOptions)); 
}

function push_to_cdn_return_url_to_client($fileName, $tempURL, $container, $recordingType)
{
    /*upload to rackspace CDN*/
    try{
        $custom_headers = array('Access-Control-Allow-Origin' => '*');          //allow JS/ ajax calls to pull url
        $container->uploadObject($fileName, $tempURL, $custom_headers);

        while(!$container->objectExists($fileName)){}  //kill time until upload complete

        $obj = $container->getObject($fileName);

        $temp = $obj->getTemporaryUrl(120, 'GET');

        echo '{"status": "found", "filename":"'.$temp.'", "recordingType": "'.$recordingType.'"}';
        exit();
         
    }
    catch(Exception $e)
    {
        echo '{"status": "error", "message": "'.$e->getMessage().'","recordingType": "none", "string": "'.$fileName.'", "filename": "'.$tempURL.'"}';
        exit();
    } 
}
?>